﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models
{
    public class Reservation
    {
        public int ReservationId { get; set; }
        public DateTime ReservationTime { get; set; }
        public DateTime? ReturingTime { get; set; }
        public DateTime? RecievmentTime { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }

    }
}

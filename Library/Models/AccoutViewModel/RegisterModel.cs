﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models.AccoutViewModel
{
    public class RegisterModel
    {
        [Required]
        [EmailAddress]
        [Display(Name ="Адрес электронной почты")]
        public string Email { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        [Display(Name = "Пароль")]
        [MinLength(7,ErrorMessage ="Минимальная длина пароля-6 символов")]
        [MaxLength(20,ErrorMessage ="Максимальная длина пароля-20 символов")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}

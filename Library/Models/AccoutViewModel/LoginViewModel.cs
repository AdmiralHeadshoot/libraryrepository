﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models.AccoutViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="Не указан электронный адрес")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage ="Не указан пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        [Display(Name = "Запомнить пароль")]
        public bool RememberPassword { get; set; }
        public string ReturnUrl { get; internal set; }
    }
}

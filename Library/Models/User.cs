﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;


namespace Library.Models
{
    public class User : IdentityUser
    {
        public ICollection<Reservation> Reservations { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }
        public static object Identity { get; internal set; }

        public User()
        {
            Reservations = new List<Reservation>();
            Subscriptions = new List<Subscription>();
        }
    }

}


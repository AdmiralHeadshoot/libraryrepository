﻿namespace Library.ViewModels
{
    public class GenreSelectListViewModel
    {
        public int GenreId { get; set; }
        public string GenreName { get; set; }
    }
}

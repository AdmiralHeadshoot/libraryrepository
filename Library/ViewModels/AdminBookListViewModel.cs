﻿using Library.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class AdminBookListViewModel
    {
        public BooksFilterViewModel BooksFilterViewModel { get; set; }
        public IEnumerable<BookSelectViewModel> Books { get; set; }
        //[Display(Name = "Автор:")]
        //public SelectList Authors { get; set; }
        //[Display(Name = "Издатель:")]
        //public SelectList Publishers { get; set; }
        //[Display(Name = "Жанр")]
        //public SelectList Genres { get; set; }
        //public List<Book> Books { get; set; }
        //public User User { get; set; }
        //public string SearchString { get; set; }
    }
}

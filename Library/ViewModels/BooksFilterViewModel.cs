﻿using Library.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

using System.Collections.Generic;

namespace Library.ViewModels
{
    public class BooksFilterViewModel
    {
        public SelectList Authors { get; set; }
        public SelectList Publishers { get; set; }
        public SelectList Genres { get; set; }
        public List<BookSelectViewModel> Books { get; set; }
        public User User { get; set; }
        public string SearchString { get; set; }       
        public int? SelectedAuthor { get; private set; }
        public int? SelectedPublisher { get; private set; }
        public int? SelectedGenre { get; private set; }
       

        public BooksFilterViewModel(List<AuthorSelectListViewModel> authors, int? author, List<GenreSelectListViewModel> genres, int? genre, List<PublisherSelectListViewModel> publishers, int? publisher, string name)
        {
            //var configAuthor = new MapperConfiguration(cfg => cfg.CreateMap<Author, AuthorSelectListViewModel>()
            //.ForMember(dest=>dest.AuthorId,opt=>opt.MapFrom(a=>a.AuthorId)).
            //ForMember(dest=>dest.AuthorName,opt=>opt.MapFrom(a=>a.AuthorName)));
            //var mapperAuthor = new Mapper(configAuthor);
            //List<AuthorSelectListViewModel> AuthorSelectList = mapperAuthor.Map<List<Author>, List<AuthorSelectListViewModel>>(authors);
            //List<AuthorSelectListViewModel>AuthorSelectList = mapper.Map<List<Author>, List<AuthorSelectListViewModel>>(authors);
            

            //var configGenre = new MapperConfiguration(cfg => cfg.CreateMap<Genre, GenreSelectListViewModel>()
            //.ForMember(dest=>dest.GenreId,opt=>opt.MapFrom(g=>g.GenreId))
            //.ForMember(dest=>dest.GenreName,opt=>opt.MapFrom(g=>g.GenreName)));
            //var mapperGenre = new Mapper(configGenre);
            //List<GenreSelectListViewModel> GenreSelectList = mapperGenre.Map<List<Genre>, List<GenreSelectListViewModel>>(genres);

            //var configPublisher = new MapperConfiguration(cfg => cfg.CreateMap<Publisher, PublisherSelectListViewModel>()
            //  .ForMember(dest => dest.PublisherId, opt => opt.MapFrom(p => p.PublisherId))
            //  .ForMember(dest => dest.PublisherName, opt => opt.MapFrom(p => p.PublisherName)));
            //var mapperPublisher = new Mapper(configPublisher);
            //List<PelectListViewModel> PublisherSelectList = mapperPublisher.Map<List<Publisher>, List<PublisherSelectListViewModel>>(publishers);

            ////////////////
            //genres.Insert(0, new GenreSelectListViewModel { GenreName = "Все", GenreId = 0 });
            //Genres = new SelectList(GenreSelectList, "GenreId", "GenreName", genre);
            //SelectedGenre = genre;

            //AuthorSelectList.Insert(0, new AuthorSelectListViewModel { AuthorName = "Все", AuthorId = 0 });
            //Authors = new SelectList(AuthorSelectList, "AuthorId", "AuthorName", author);
            //SelectedAuthor = author;

            //PublisherSelectList.Insert(0, new PublisherSelectListViewModel { PublisherName = "Все", PublisherId = 0 });
            //Publishers = new SelectList(PublisherSelectList, "PublisherId", "PublisherName", publisher);
            SelectedPublisher = publisher;

            ///////////////

            authors.Insert(0, new AuthorSelectListViewModel { AuthorName = "Все", AuthorId = 0 });
            Authors = new SelectList(authors, "AuthorId", "AuthorName", author);
            SelectedAuthor = author;

            genres.Insert(0, new GenreSelectListViewModel { GenreName = "Все", GenreId = 0 });
            Genres = new SelectList(genres, "GenreId", "GenreName", genre);
            SelectedGenre = genre;

            publishers.Insert(0, new PublisherSelectListViewModel { PublisherName = "Все", PublisherId = 0 });
            Publishers = new SelectList(publishers, "PublisherId", "PublisherName", publisher);
            SelectedPublisher = publisher;

            SearchString = name;
        }
    }
}

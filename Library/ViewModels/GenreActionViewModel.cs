﻿namespace Library.ViewModels
{
    public class GenreActionViewModel
    {
        public int Id { get; set; }
        public string GenreName { get; set; }
    }
}

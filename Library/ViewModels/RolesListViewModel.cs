﻿namespace Library.ViewModels
{
    public class RolesListViewModel
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
    }
}

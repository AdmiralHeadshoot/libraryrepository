﻿using System;

namespace Library.ViewModels
{
    public class ReservationListViewModel
    {
        public int ReservationId { get; set; }
        public DateTime ReservationTimeView { get; set; }
        public string BookName { get; set; }
        public string UserEmail{ get; set; }
    }
}

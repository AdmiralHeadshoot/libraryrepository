﻿using Library.Models;
using System.Collections.Generic;

namespace Library.ViewModels
{
    public class DeleteAuthorViewModel
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public List<Book> Books { get; set; } 
    }
}

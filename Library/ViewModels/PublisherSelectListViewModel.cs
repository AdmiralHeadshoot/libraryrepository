﻿namespace Library.ViewModels
{
    public class PublisherSelectListViewModel
    {
        public int PublisherId { get; set; }
        public string PublisherName { get; set; }
    }
}

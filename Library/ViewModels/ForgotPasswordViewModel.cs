﻿using System.ComponentModel.DataAnnotations;

namespace Library.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Library.ViewModels
{
    public class MyPageViewModel
    {
        public IEnumerable<ReservationListViewModel> Reservations { get; set; }
        public IEnumerable<BookingsListViewModel> Bookings { get; set; }
    }
}

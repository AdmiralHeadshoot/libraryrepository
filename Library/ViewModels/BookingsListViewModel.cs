﻿using System;

namespace Library.ViewModels
{
    public class BookingsListViewModel
    {
        public int Id { get; set; }
        public string BookName { get; set; }
        public DateTime RecievmentTimeView { get; set; }
        public string UserName { get; set; }
        public DateTime ReturingTimeView { get; set; }
    }
}

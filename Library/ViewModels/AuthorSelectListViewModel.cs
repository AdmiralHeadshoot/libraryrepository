﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class AuthorSelectListViewModel
    {
        public int AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}

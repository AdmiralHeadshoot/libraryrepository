﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class DeleteBookViewModel
    {
        public int BookId { get; set; }
        public string BookName { get; set; }
        public string GenreName { get; set; }
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }
    }
}

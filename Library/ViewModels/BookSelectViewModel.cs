﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class BookSelectViewModel
    {
        public int Id { get; set; }
        public string NameBook { get; set; }
        public string AuthorName { get; set; }
        public int AuthorId { get; set; }
        public string GenreName { get; set; }
        public int GenreId { get; set; }
        public string PublisherName { get; set; }
        public int PublisherId { get; set; }
        public string Picture { get; set; }
        public bool IsAvailable { get; set; }
    }
}

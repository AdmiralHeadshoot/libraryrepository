﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Library.ViewModels
{
    public class ChangeUserRoleViewModel
    {
        public string Id { get; set; }
        public List<IdentityRole> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }
        public ChangeUserRoleViewModel()
        {
            AllRoles = new List<IdentityRole>();
            UserRoles = new List<string>();
        }
    }
}

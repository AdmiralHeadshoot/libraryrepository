﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class MyReservationsViewModel
    {
        public List<Models.Reservation> Reservations { get; set; }
        public List<Models.Reservation> Bookings { get; set; }
    }
}

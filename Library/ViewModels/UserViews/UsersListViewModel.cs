﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class UsersListViewModel
    {
        public string UserId { get; set; }
        public string UserEmail { get; set; }
    }
}

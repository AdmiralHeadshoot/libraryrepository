﻿using Library.Models;
using System.Collections.Generic;

namespace Library.ViewModels
{
    public class CatalogViewModel
    {
        public BooksFilterViewModel BooksFilterViewModel { get; set; }
        public List<BookSelectViewModel> Books { get; set; }
        public PageViewModel PageViewModel { get; set; }      
        public User User { get; set; }
    }
}

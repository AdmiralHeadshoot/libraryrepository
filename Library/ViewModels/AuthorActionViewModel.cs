﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ViewModels
{
    public class AuthorActionViewModel
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
    }
}

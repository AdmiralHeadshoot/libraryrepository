﻿using MimeKit;
using MailKit.Net.Smtp;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Library.Services
{
    public class EmailService:PageModel
    {
        private readonly IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var Password = _configuration["EmailServiceAuth:Password"];
            var Email = _configuration["EmailServiceAuth:Email"];
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Библиотека", Email));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_configuration["SmtpServer:Name"], Convert.ToInt32(_configuration["SmtpServer:Port"]), false);
                await client.AuthenticateAsync(Email, Password);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}

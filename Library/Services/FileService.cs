﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace Library.Services
{
    public class FileService
    {
        public async Task AddFileAsync(IFormFile file, string FolderPath, IWebHostEnvironment _appEnvironvent)
        {
            string filePath = "/" + FolderPath + "/" + file.FileName;
            using (var stream = new FileStream(_appEnvironvent.WebRootPath + filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
        }
    }
}

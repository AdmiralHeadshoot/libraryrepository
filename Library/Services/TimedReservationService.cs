﻿using Library.Models;
using Library.Persistence;
using Library.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Library.Services
{
    public class TimedReservationService : IHostedService, IDisposable
    {
        private Timer timer;
        private readonly IServiceScopeFactory scopeFactory;
        private readonly IConfiguration _configuration;
        private readonly IReservationRepository _reservationRepository;
        private readonly IUnitOfWork _unitOfWork;
        private TimedReservationService(IServiceScopeFactory scopeFactory,IConfiguration configuration,IReservationRepository reservationRepository,IUnitOfWork unitOfWork)
        {
            this.scopeFactory = scopeFactory;
            _configuration = configuration;
            _reservationRepository = reservationRepository;
            _unitOfWork = unitOfWork;
        }
        public void Dispose()
        {
            timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(Dowork, null, TimeSpan.Zero, TimeSpan.FromMinutes(5));
            return Task.CompletedTask;
        }
        private void Dowork(object state)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                var contextReservation = scope.ServiceProvider.GetRequiredService<IReservationRepository>();
                var contextBook = scope.ServiceProvider.GetRequiredService<IBookRepository>();
                var contextSubscription = scope.ServiceProvider.GetRequiredService<ISubscriptionRepository>();
              
                IEnumerable<Reservation> reservations = _reservationRepository.GetTimeOutReservationsList().ToList();
                foreach (var r in reservations)
                {
                    Book book = contextBook.GetItem(r.BookId);
                    book.isAvailable = true;
                    contextBook.Update(book);
                    contextReservation.Delete(r.BookId);
                    _unitOfWork.SaveContext();
                    List<Subscription> subscriptions = contextSubscription.GetItemList(book.Id).ToList();
                    SendEmail(subscriptions);
                }
            }
        }
        public void SendEmail(List<Subscription> subscriptions)
        {
            EmailService emailService = new EmailService(_configuration);
            foreach (var e in subscriptions)
            {
                string email = e.User.Email;
                string message = $"Книгу {e.Book.Name} можно забронировать";
                string subject = "Книга, на обновления которой вы подписаны, доступна для бронирования";
                emailService.SendEmailAsync(email, subject, message);        
            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
    }
}

﻿using Library.Models;
using Microsoft.AspNetCore.Identity;

namespace Library.Persistence
{
    public class UserInitializer
    {
        public static void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }
        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("admin").Result)
            {
                IdentityRole admin = new IdentityRole { Name = "admin" };
                IdentityResult result = roleManager.CreateAsync(admin).Result;
            }
            if (!roleManager.RoleExistsAsync("user").Result)
            {
                IdentityRole user = new IdentityRole { Name = "user" };
                IdentityResult result = roleManager.CreateAsync(user).Result;
            }
            if (!roleManager.RoleExistsAsync("librarian").Result)
            {
                IdentityRole librarian = new IdentityRole { Name = "librarian" };
                IdentityResult result = roleManager.CreateAsync(librarian).Result;
            }
        }
        public static void SeedUsers(UserManager<User> userManager)
        {
            if (userManager.FindByNameAsync("akakiy1983@inbox.ru").Result == null)
            {
                User admin = new User { Email = "akakiy1983@inbox.ru", UserName = "akakiy1983@inbox.ru", EmailConfirmed = true };
                IdentityResult result = userManager.CreateAsync(admin, "Admin0000").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(admin, "admin").Wait();
                }
            }
            if (userManager.FindByNameAsync("librarian@mail.ru").Result == null)
            {
                User librarian = new User { Email = "librarian@mail.ru", UserName = "librarian@mail.ru", EmailConfirmed = true };
                IdentityResult result = userManager.CreateAsync(librarian, "Librarian0000").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(librarian, "librarian").Wait();
                }
            }
        }
    }
}

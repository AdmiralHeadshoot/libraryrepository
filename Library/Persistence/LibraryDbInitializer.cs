﻿using System.Linq;
using Library.Models;

namespace Library.Persistence
{
    public class LibraryDbInitializer
    {
        public void Initialize(DataContext context)
        {
            if (context.Books.Any() && context.Authors.Any() && context.Genres.Any() && context.Publishers.Any())
            {
                return;
            }
            context.Database.EnsureCreated();
            Author Pus = new Author { AuthorName = "А.С.Пушкин" };
            Author Cheh = new Author { AuthorName = "А.П.Чехов" };
            Author Kril = new Author { AuthorName = "И.А.Крылов" };
            Author Tols = new Author { AuthorName = "Л.Н.Толстой" };
            Author Rouling = new Author { AuthorName = "Дж.К.Роулинг" };
            Author King = new Author { AuthorName = "Стивен Кинг" };
            Author Kuper = new Author { AuthorName = "Джеймс Фенимор Купер" };
            Author Vern = new Author { AuthorName = "Жуль Верн" };

            Genre roman = new Genre { GenreName = "Роман" };
            Genre basnya = new Genre { GenreName = "Басня" };
            Genre raskaz = new Genre { GenreName = "Рассказ" };
            Genre fant = new Genre { GenreName = "Фентези" };
            Genre pies = new Genre { GenreName = "Пьеса" };

            Publisher Kazan = new Publisher { PublisherName = "Казан" };
            Publisher Eksmo = new Publisher { PublisherName = "Эксмо" };
            Publisher Classic = new Publisher { PublisherName = "Классик" };
            Publisher AST = new Publisher { PublisherName = "АСТ" };
            Publisher Ros = new Publisher { PublisherName = "Росмэн" };
            Publisher BBP = new Publisher { PublisherName = "Большая библиотека приключений" };
            Publisher Bom = new Publisher { PublisherName = "Бомбора" };
            Publisher ISK = new Publisher { PublisherName = "ИСК" };

            Book book1 = new Book { Name = "Капитанская дочка", Author = Pus, Genre = roman, Publisher = Kazan, Picture = "Капитанская дочка-Эксмо.jpg" };
            Book book2 = new Book { Name = "Гарри Поттер и Философский камень", Author = Rouling, Genre = fant, Publisher = Ros, Picture = "Гарри Поттер и философский камень-Росмэн.jpg" };
            Book book3 = new Book { Name = "Гарри Поттер и Тайная комната", Author = Rouling, Genre = fant, Publisher = Ros, Picture = "Гарри Поттер и тайная комната-Росмэн.jpg" };
            Book book4 = new Book { Name = "Гарри Поттер и Узник Азкабана", Author = Rouling, Genre = fant, Publisher = Ros, Picture = "Гарри Поттер и узник азкабана-Росмэн.jpg" };
            Book book5 = new Book { Name = "Гарри Поттер и Кубок Огня", Author = Rouling, Genre = fant, Publisher = Ros, Picture = "Гарри Поттер и кубок огня-Росмэн.jpg" };
            Book book6 = new Book { Name = "Гарри Поттер и Орден феникса", Author = Rouling, Genre = fant, Publisher = Ros, Picture = "Гарри Поттер и орден феникса-Росмэн.jpg" };
            Book book7 = new Book { Name = "Гарри Поттер и Принц полукровка", Author = Rouling, Genre = fant, Publisher = Ros, Picture = "Гарри Поттер и принц полукровка-Росмэн.jpg" };
            Book book8 = new Book { Name = "Краткие ответы на большие вопросы", Author = Rouling, Genre = fant, Publisher = Ros, Picture = "Краткие ответы на большие вопросы.jpg" };
            Book book9 = new Book { Name = "Война и мир", Author = Tols, Genre = roman, Publisher = Eksmo, Picture = "Война и мир-Эксмо.jpeg" };
            Book book10 = new Book { Name = "Вишневый сад", Author = Cheh, Genre = pies, Publisher = ISK, Picture = "Вишневый сад-ИСК.jpg" };
            Book book11 = new Book { Name = "Зеленая миля", Author = King, Genre = roman, Publisher = AST, Picture = "Зеленая миля-АСТ.jpg" };
            Book book12 = new Book { Name = "Зверобой", Author = Kuper, Genre = roman, Publisher = BBP, Picture = "Зверобой-Большая библиотека приключений.jpg" };
            Book book13 = new Book { Name = "20000 лье под водой", Author = Vern, Genre = roman, Publisher = BBP, Picture = "20000 Лье под водой-Большая библиотека приключений.jpg" };

            context.Authors.AddRange(Pus, Cheh, Kril, Tols, Rouling, King, Kuper, Vern);
            context.Genres.AddRange(roman, basnya, raskaz, pies, fant);
            context.Publishers.AddRange(Kazan, Eksmo, Classic, AST, Ros, BBP, Bom, ISK);
            context.Books.AddRange(book1, book2, book3, book4, book5, book6, book7, book8, book9, book10, book11, book12, book13);
            context.SaveChanges();
        }
    }
}

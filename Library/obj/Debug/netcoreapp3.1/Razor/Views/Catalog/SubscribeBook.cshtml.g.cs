#pragma checksum "C:\Users\Olekt\Source\Repos\libraryrepository\Library\Views\Catalog\SubscribeBook.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "36cb26b360e694e707afc5fe69c21214ac68a26b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Catalog_SubscribeBook), @"mvc.1.0.view", @"/Views/Catalog/SubscribeBook.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Olekt\Source\Repos\libraryrepository\Library\Views\_ViewImports.cshtml"
using Library;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Olekt\Source\Repos\libraryrepository\Library\Views\_ViewImports.cshtml"
using Library.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"36cb26b360e694e707afc5fe69c21214ac68a26b", @"/Views/Catalog/SubscribeBook.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8b427a1b232822b14afc1adf4a2f0d95d7d90ea4", @"/Views/_ViewImports.cshtml")]
    public class Views_Catalog_SubscribeBook : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Library.ViewModels.BookSelectViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\Olekt\Source\Repos\libraryrepository\Library\Views\Catalog\SubscribeBook.cshtml"
   
    ViewData["Title"] = "Подписка на обновления";

#line default
#line hidden
#nullable disable
            WriteLiteral("<h2>Вы подписались на получение уведомлений по книге ");
#nullable restore
#line 5 "C:\Users\Olekt\Source\Repos\libraryrepository\Library\Views\Catalog\SubscribeBook.cshtml"
                                                Write(Model.NameBook);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\r\n");
#nullable restore
#line 6 "C:\Users\Olekt\Source\Repos\libraryrepository\Library\Views\Catalog\SubscribeBook.cshtml"
Write(Html.ActionLink("Каталог","BooksList"));

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Library.ViewModels.BookSelectViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.ViewModels;
using Library.Models;
using System.Threading;
using Library.Repositories;

namespace Library.Features.PublisherFeatures
{
    public class DeleteConfirmed:IRequest<Publisher>
    {
        public int Id;
        public class DeleteConfirmedHandler : IRequestHandler<DeleteConfirmed, Publisher>
        {
            private IPublisherRepository _publisherRepository;
            private IUnitOfWork _unitOfWork;
            public DeleteConfirmedHandler(IPublisherRepository publisherRepository,IUnitOfWork unitOfWork)
            {
                _publisherRepository = publisherRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<Publisher> Handle(DeleteConfirmed request, CancellationToken cancellationToken)
            {
                Publisher publisher = await _publisherRepository.GetItemAsync(request.Id);               
                _publisherRepository.Delete(request.Id);
                _unitOfWork.SaveContext();
                return publisher;
            }
        }
    }
}

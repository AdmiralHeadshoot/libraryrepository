﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;
using MediatR;
using Library.ViewModels;
using System.Threading;
using Library.Repositories;

namespace Library.Features.PublisherFeatures
{
    public class EditPublisherGet : IRequest<PublisherSelectListViewModel>
    {
        public int Id;
        public class EditPublisherGetHandler : IRequestHandler<EditPublisherGet, PublisherSelectListViewModel>
        {
            private IPublisherRepository _publisherRepository;
            public EditPublisherGetHandler(IPublisherRepository publisherRepository)
            {
                _publisherRepository = publisherRepository;
            }

            public async Task<PublisherSelectListViewModel> Handle(EditPublisherGet request, CancellationToken cancellationToken)
            {
                var Publisher = await _publisherRepository.GetItemAsync(request.Id);
                PublisherSelectListViewModel model = new PublisherSelectListViewModel
                {
                    PublisherId = Publisher.PublisherId,
                    PublisherName = Publisher.PublisherName
                };
                return model;
            }
            //public async Task<PublisherSelectListViewModel> Handle(EditPublisherGet request, CancellationToken cancellationToken)
            //{
            //    var Publisher = _publisherRepository.GetItem(request.Id);
            //    PublisherSelectListViewModel model = new PublisherSelectListViewModel
            //    {
            //        PublisherId = Publisher.PublisherId,
            //        PublisherName = Publisher.PublisherName
            //    };
            //    return model;
            //}
        }
    }
}

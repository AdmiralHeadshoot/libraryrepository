﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using Library.ViewModels;
using System.Threading;
using Library.Models;

namespace Library.Features.PublisherFeatures
{
    public class EditPublisherPost:IRequest<Publisher>
    {
        public Publisher publisher;
        public class EditPublisherPostHandler : IRequestHandler<EditPublisherPost, Publisher>
        {
            private IPublisherRepository _publisherRepository;
            private IUnitOfWork _unitOfWork;
            public EditPublisherPostHandler(IPublisherRepository publisherRepository,IUnitOfWork unitOfWork)
            {
                _publisherRepository = publisherRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<Publisher> Handle(EditPublisherPost request, CancellationToken cancellationToken)
            {
                _publisherRepository.Update(request.publisher);
                _unitOfWork.SaveContext();
                return request.publisher;
            }
        }
    }
}

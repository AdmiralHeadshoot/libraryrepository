﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Repositories;
using MediatR;
using Library.Models;
using System.Threading;
using Library.ViewModels;

namespace Library.Features.PublisherFeatures
{
    public class FindByIdPublisher : IRequest<PublisherSelectListViewModel>
    {
        public int Id;
        public class FindByIdPublisherHandler : IRequestHandler<FindByIdPublisher, PublisherSelectListViewModel>
        {
            private IPublisherRepository _publisherRepository;
            public FindByIdPublisherHandler(IPublisherRepository publisherRepository)
            {
                _publisherRepository = publisherRepository;
            }
            public async Task<PublisherSelectListViewModel> Handle(FindByIdPublisher request, CancellationToken cancellationToken)
            {
                Publisher Publisher = await _publisherRepository.GetItemAsync(request.Id);
                PublisherSelectListViewModel model = new PublisherSelectListViewModel
                {
                    PublisherId=Publisher.PublisherId,
                    PublisherName=Publisher.PublisherName
                };
                return model;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;
using MediatR;
using Library.Repositories;
using System.Threading;
using Library.ViewModels;
using AutoMapper;

namespace Library.Features.PublisherFeatures
{
    public class PublisherList:IRequest<IEnumerable<PublisherSelectListViewModel>>
    {
        public class PublisherListHandler : IRequestHandler<PublisherList, IEnumerable<PublisherSelectListViewModel>>
        {
            private readonly IPublisherRepository _publisherRepository;
            private readonly IMapper _mapper;
            public PublisherListHandler(IPublisherRepository publisherRepository,IMapper mapper)
            {
                _publisherRepository = publisherRepository;
                _mapper = mapper;
            }
            public async Task<IEnumerable<PublisherSelectListViewModel>> Handle(PublisherList request, CancellationToken cancellationToken)
            {
                IEnumerable<PublisherSelectListViewModel> publishers = _mapper.Map<List<Publisher>, List<PublisherSelectListViewModel>>(await _publisherRepository.GetLItem());
                return publishers;
            }
        }
    }
}

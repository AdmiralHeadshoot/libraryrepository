﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Library.Repositories;
using Library.ViewModels;
using System.Threading;
using Library.Models;

namespace Library.Features.PublisherFeatures
{
    public class AddPublisher:IRequest<PublisherSelectListViewModel>
    {
        public Publisher publisher { get; set; }
        public class AddPubliserHandler : IRequestHandler<AddPublisher, PublisherSelectListViewModel>
        {
            private readonly IPublisherRepository _publisherRepository;
            private readonly IUnitOfWork _unitOfWork;
            public AddPubliserHandler(IPublisherRepository publisherRepository,IUnitOfWork unitOfWork)
            {
                _publisherRepository = publisherRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<PublisherSelectListViewModel> Handle(AddPublisher request, CancellationToken cancellationToken)
            {
                PublisherSelectListViewModel model = new PublisherSelectListViewModel
                {
                    PublisherId = request.publisher.PublisherId,
                    PublisherName = request.publisher.PublisherName
                };
                _publisherRepository.Create(request.publisher);
                _unitOfWork.SaveContext();
                return model;
            }
        }
    }
}

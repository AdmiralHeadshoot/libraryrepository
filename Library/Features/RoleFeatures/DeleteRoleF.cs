﻿using System.Threading.Tasks;
using MediatR;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using AutoMapper;

namespace Library.Features.RoleFeatures
{
    public class DeleteRoleF:IRequest<IdentityRole>
    {
        public string Id;
        public class DeleteRoleFHandler : IRequestHandler<DeleteRoleF, IdentityRole>
        {
            private RoleManager<IdentityRole> _roleManager;
            public DeleteRoleFHandler(RoleManager<IdentityRole> roleManager)
            {
                _roleManager = roleManager;
            }
            public async Task<IdentityRole> Handle(DeleteRoleF request, CancellationToken cancellationToken)
            {
                IdentityRole role = await _roleManager.FindByIdAsync(request.Id);
                if (role != null)
                {
                    await _roleManager.DeleteAsync(role);
                }
                return role;
            }
        }
    }
}

﻿using System.Threading.Tasks;
using MediatR;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.RoleFeatures
{
    public class CreateRoleF:IRequest<IdentityResult>
    {
        public string Name;
        public class CreateRoleFHandler : IRequestHandler<CreateRoleF, IdentityResult>
        {
            private readonly RoleManager<IdentityRole> _roleManager;
            public CreateRoleFHandler(RoleManager<IdentityRole> roleManager)
            {
                _roleManager = roleManager;
            }
            public async Task<IdentityResult> Handle(CreateRoleF request, CancellationToken cancellationToken)
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(request.Name));
                return result;
            }
        }
    }
}

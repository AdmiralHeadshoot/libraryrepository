﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using Library.ViewModels;

namespace Library.Features.RoleFeatures
{
    public class RoleListF:IRequest<List<RolesListViewModel>>
    {
        public class RoleListFHandler : IRequestHandler<RoleListF, List<RolesListViewModel>>
        {
            private RoleManager<IdentityRole> _roleManager;
            private IMapper _mapper;
            public RoleListFHandler(RoleManager<IdentityRole> roleManager,IMapper mapper)
            {
                _roleManager = roleManager;
                _mapper = mapper;
            }
            public async Task<List<RolesListViewModel>> Handle(RoleListF request, CancellationToken cancellationToken)
            {
                List<RolesListViewModel> roles  = _mapper.Map<List<IdentityRole>, List<RolesListViewModel>>(_roleManager.Roles.ToList());
                return roles;
            }
        }
    }
}

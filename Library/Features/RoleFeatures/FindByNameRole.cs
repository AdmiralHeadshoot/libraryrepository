﻿using System.Threading.Tasks;
using Library.ViewModels;
using MediatR;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.RoleFeatures
{
    public class FindByNameRole:IRequest<RolesListViewModel>
    {
        public string Name;
        public class FindByNameRoleHandler : IRequestHandler<FindByNameRole, RolesListViewModel>
        {
            private readonly RoleManager<IdentityRole> _roleManager;
            public FindByNameRoleHandler(RoleManager<IdentityRole> roleManager)
            {
                _roleManager = roleManager;
            }
            public async Task<RolesListViewModel> Handle(FindByNameRole request, CancellationToken cancellationToken)
            {
                IdentityRole roleName = await _roleManager.FindByNameAsync(request.Name);
                if (roleName == null)
                {
                    RolesListViewModel model = new RolesListViewModel
                    {
                        RoleName=request.Name
                    };
                    return model;
                }
                return null;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.RoleFeatures
{
    public class EditUserRolePostF : IRequest<User>
    {
        public string UserId;
        public List<string> Roles;
        public class EditUserRolePostFHandler : IRequestHandler<EditUserRolePostF, User>
        {
            private readonly RoleManager<IdentityRole> _roleManager;
            private readonly UserManager<User> _userManager;
            public EditUserRolePostFHandler(RoleManager<IdentityRole> roleManager, UserManager<User> userManager)
            {
                _roleManager = roleManager;
                _userManager = userManager;
            }

            public async Task<User> Handle(EditUserRolePostF request, CancellationToken cancellationToken)
            {
                User user = await _userManager.FindByIdAsync(request.UserId);
                if (user != null)
                {
                    var userRoles = await _userManager.GetRolesAsync(user);
                    var allRoles = _roleManager.Roles.ToList();
                    var addedRoles = request.Roles.Except(userRoles);
                    var removeRoles = userRoles.Except(request.Roles);
                    await _userManager.AddToRolesAsync(user, addedRoles);
                    return user;
                }
                return null;
            }
        }
    }
}

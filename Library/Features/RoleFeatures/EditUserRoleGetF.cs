﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using Library.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.RoleFeatures
{
    public class EditUserRoleGetF:IRequest<ChangeUserRoleViewModel>
    {
        public string UserId;
        public class EditUserRoleFHandler : IRequestHandler<EditUserRoleGetF, ChangeUserRoleViewModel>
        {
            private readonly UserManager<User> _userManager;
            private readonly RoleManager<IdentityRole> _roleManager;
            public EditUserRoleFHandler(UserManager<User> userManager,RoleManager<IdentityRole> roleManager)
            {
                _userManager = userManager;
                _roleManager = roleManager;
            }
            public async Task<ChangeUserRoleViewModel> Handle(EditUserRoleGetF request, CancellationToken cancellationToken)
            {
                User user = await _userManager.FindByIdAsync(request.UserId);
                if (user != null)
                {
                    var userRoles = await _userManager.GetRolesAsync(user);
                    var allRoles = _roleManager.Roles.ToList();
                    ChangeUserRoleViewModel model = new ChangeUserRoleViewModel
                    {
                        Id = user.Id,
                        UserRoles = userRoles,
                        AllRoles = allRoles
                    };
                    return model;
                }
                return null;
            }
        }
    }
}

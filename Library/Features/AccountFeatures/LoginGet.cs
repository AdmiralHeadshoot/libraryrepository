﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models.AccoutViewModel;
using Library.ViewModels;
using System.Threading;

namespace Library.Features.AccountFeatures
{
    public class LoginGet : IRequest<LoginViewModel>
    {
        public string returnUrl { get; set; }
        public class LoginGetHandler : IRequestHandler<LoginGet, LoginViewModel>
        {
            public async Task<LoginViewModel> Handle(LoginGet request, CancellationToken cancellationToken)
            {
                LoginViewModel model = new LoginViewModel { ReturnUrl = request.returnUrl };
                return model;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models.AccoutViewModel;
using Library.ViewModels;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Library.Models;
using Microsoft.AspNetCore.Mvc;

namespace Library.Features.AccountFeatures
{
    public class LoginPost : IRequest<LoginViewModel>
    {
        public LoginViewModel model;
        public string returnUrl;
        public IUrlHelper _url;
        public class LoginPostHandler : IRequestHandler<LoginPost, LoginViewModel>
        {
            private readonly UserManager<User> _userManager;
            private readonly SignInManager<User> _signInManager;
            private readonly IMediator _mediator;
            public LoginPostHandler(IMediator mediator, UserManager<User> userManager, SignInManager<User> signInManager)
            {
                _userManager = userManager;
                _signInManager = signInManager;
                _mediator = mediator;
            }
            public async Task<LoginViewModel> Handle(LoginPost request, CancellationToken cancellationToken)
            {
                User user = new User { Email = request.model.Email, UserName = request.model.Email };
                var result = await _signInManager.PasswordSignInAsync(request.model.Email, request.model.Password, request.model.RememberPassword, false);
                //if (result.Succeeded)
                //{              
                    //{
                    //    return Redirect(request.model.ReturnUrl);
                    //}
                    //else
                    //{
                    //    return RedirectToAction("BooksList", "Catalog");
                    //}
                //}
                return request.model;
             }
        }
    }
}

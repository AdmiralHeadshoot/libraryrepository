﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using Library.ViewModels;
using MediatR;
using Library.Repositories;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.AccountFeatures
{
    public class FindByEmailUser:IRequest<User>
    {
        public ForgotPasswordViewModel model { get; set; }
        public class FindByEmailUserHandler : IRequestHandler<FindByEmailUser, User>
        {
            private readonly UserManager<User> _userManager;
            public FindByEmailUserHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public async Task<User> Handle(FindByEmailUser request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByEmailAsync(request.model.Email);
                return user;
            }
        }
    }
}

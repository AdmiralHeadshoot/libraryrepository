﻿using System.Threading.Tasks;
using MediatR;
using Library.Models.AccoutViewModel;
using Library.Models;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Library.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Library.Features.AccountFeatures
{
    public class Register:IRequest<RegisterModel>
    {
        public RegisterModel registerModel { get; set; }
        public HttpContext httpContext;
        public IUrlHelper _url;
        
        public class RegisterHandler : IRequestHandler<Register, RegisterModel>
        {
            private readonly UserManager<User> _userManager;
            private readonly SignInManager<User> _signInManager;
            public readonly Microsoft.Extensions.Configuration.IConfiguration _configuration;
            public RegisterHandler(UserManager<User> userManager,SignInManager<User> signInManager, Microsoft.Extensions.Configuration.IConfiguration configuration)
            {
                _userManager = userManager;
                _signInManager = signInManager;
                _configuration = configuration;
            }
            public async Task<RegisterModel> Handle(Register request, CancellationToken cancellationToken)
            {
                User user = new User { Email = request.registerModel.Email, UserName = request.registerModel.Email };
                var result = await _userManager.CreateAsync(user, request.registerModel.Password);

                if (result.Succeeded)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl =request._url.Action("ConfrimEmail", "Account", new { userId = user.Id, code = code }, protocol: request.httpContext.Request.Scheme);
                    EmailService emailService = new EmailService(_configuration);

                    await emailService.SendEmailAsync(request.registerModel.Email, "Подтвердите регистрацию в библиотеке", $"Для подтверждения регистрации <a href='{callbackUrl}'>перейдите по ссылке</a>");
                    await _signInManager.SignInAsync(user, false);
                }
                return request.registerModel;
            }
        }
    }
}

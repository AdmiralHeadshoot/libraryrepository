﻿using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.AccountFeatures
{
    public class ResetPasswordF:IRequest<IdentityResult>
    {
        public User user;
        public string Code;
        public string Password;
        public class ResetPasswordFHandler : IRequestHandler<ResetPasswordF, IdentityResult>
        {
            private readonly UserManager<User> _userManager;
            public ResetPasswordFHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public async Task<IdentityResult> Handle(ResetPasswordF request, CancellationToken cancellationToken)
            {
                IdentityResult result = await _userManager.ResetPasswordAsync(request.user,request.Code,request.Password);
                return result;
            }
        }
    }
}

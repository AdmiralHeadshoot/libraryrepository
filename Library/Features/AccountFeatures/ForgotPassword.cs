﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.ViewModels;
using Library.Models;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using AutoMapper.Configuration;
using Library.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Library.Features.AccountFeatures
{
    public class ForgotPassword:IRequest<ForgotPasswordViewModel>
    {
        public ForgotPasswordViewModel model { get; set; }
        public IUrlHelper _url;
        public HttpContext httpContext;
        
        public class ForgotPasswordHandler : IRequestHandler<ForgotPassword, ForgotPasswordViewModel>
        {
            private readonly UserManager<User> _userManager;
            public readonly Microsoft.Extensions.Configuration.IConfiguration _configuration;
            public ForgotPasswordHandler(UserManager<User> userManager, Microsoft.Extensions.Configuration.IConfiguration configuration)
            {
                _userManager = userManager;
                _configuration = configuration;
            }
            public async Task<ForgotPasswordViewModel> Handle(ForgotPassword request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByEmailAsync(request.model.Email);
                //if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                //{
                //    //return View("ForgotPasswordConfirmation");
                //}
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callBackUrl = request._url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: request.httpContext.Request.Scheme);
                EmailService emailService = new EmailService(_configuration);
                await emailService.SendEmailAsync(request.model.Email, "Сброс пароля", $"<a href='{callBackUrl}'>Для сброса пароля перейдите по ссылке</a>");
                return request.model;
            }
        }
    }
}

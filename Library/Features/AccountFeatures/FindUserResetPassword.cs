﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.AccountFeatures
{
    public class FindUserResetPassword:IRequest<User>
    {
        public string Email;
        public class FindUserResetPasswordHandler : IRequestHandler<FindUserResetPassword, User>
        {
            private readonly UserManager<User> _userManager;
            public FindUserResetPasswordHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public Task<User> Handle(FindUserResetPassword request, CancellationToken cancellationToken)
            {
                var result = _userManager.FindByEmailAsync(request.Email);
                return result;
            }
        }
    }
}

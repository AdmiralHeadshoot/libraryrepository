﻿using Library.Models;
using Library.Repositories;
using Library.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Features.AuthorFeatures
{
    public class EditAuthorGet : IRequest<AuthorActionViewModel>
    {
        public int Id { get; set; }
        public class EditAuthorHandler : IRequestHandler<EditAuthorGet, AuthorActionViewModel>
        {
            private readonly IAuthorRepository _authorRepository;
            public EditAuthorHandler(IAuthorRepository authorRepository)
            {
                _authorRepository = authorRepository;
            }

            public async Task<AuthorActionViewModel> Handle(EditAuthorGet request, CancellationToken cancellationToken)
            {
                Author author = await _authorRepository.GetItemAsync(request.Id);
                AuthorActionViewModel model = new AuthorActionViewModel
                {
                    Id = author.AuthorId,
                    AuthorName = author.AuthorName
                };
                if (author == null)
                {
                    return null;
                }
                return model;
            }


            //public Author Handle(EditAuthor request, CancellationToken cancellationToken)
            //{
            //    Author author = _authorRepository.GetItem(request.Id);
            //    if (author == null)
            //    {
            //        return null;
            //    }
            //    return author;
            //}
        }
    }
}

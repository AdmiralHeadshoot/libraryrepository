﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using Library.Models;
using Library.ViewModels;
using System.Threading;

namespace Library.Features.AuthorFeatures
{
    public class DeleteAuthorF:IRequest<DeleteAuthorViewModel>
    {
        public int Id;
        public class DeleteAuthorFHandler : IRequestHandler<DeleteAuthorF, DeleteAuthorViewModel>
        {
            private readonly IAuthorRepository _authorRepository;
            public DeleteAuthorFHandler(IAuthorRepository authorRepository)
            {
                _authorRepository = authorRepository;
            }
            public async Task<DeleteAuthorViewModel> Handle(DeleteAuthorF request, CancellationToken cancellationToken)
            {
                if (_authorRepository.GetItem(request.Id) != null)
                {
                    Author author = _authorRepository.GetAuthorWithBooks(request.Id);
                    DeleteAuthorViewModel model = new DeleteAuthorViewModel
                    {
                        Id = author.AuthorId,
                        AuthorName = author.AuthorName,
                        Books=author.Books.ToList()
                    };
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}

﻿using Library.Models;
using Library.Repositories;
using Library.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Features.AuthorFeatures
{
    public class AddAuthor:IRequest<AuthorActionViewModel>
    {
        public Author author { get; set; }
        public class AddAuthorHandler : IRequestHandler<AddAuthor, AuthorActionViewModel>
        {
            private  IAuthorRepository _authorRepository;
            private  IUnitOfWork _unitOfWork;
            public AddAuthorHandler(IAuthorRepository authorRepository,IUnitOfWork unitOfWork)
            {
                _authorRepository = authorRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<AuthorActionViewModel> Handle(AddAuthor request, CancellationToken cancellationToken)
            {
                AuthorActionViewModel model = new AuthorActionViewModel
                {
                    Id = request.author.AuthorId,
                    AuthorName = request.author.AuthorName
                };
                _authorRepository.Create(request.author);
                _unitOfWork.SaveContext();
                return model;
            }
        }
    }
}

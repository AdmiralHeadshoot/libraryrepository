﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.ViewModels;
using Library.Repositories;
using Library.Models;
using System.Threading;

namespace Library.Features.AuthorFeatures
{
    public class EditAuthorPost:IRequest<Author>
    {
        public Author author { get; set; }
        public class EditAuthorPostHandler : IRequestHandler<EditAuthorPost, Author>
        {
            private IAuthorRepository _authorRepository;
            private IUnitOfWork _unitOfWork;
            public EditAuthorPostHandler(IAuthorRepository authorRepository,IUnitOfWork unitOfWork)
            {
                _authorRepository = authorRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<Author> Handle(EditAuthorPost request, CancellationToken cancellationToken)
            {
                Author author = request.author;
                _authorRepository.Update(author);
                _unitOfWork.SaveContext();
                return author;
            }
        }
    }
}

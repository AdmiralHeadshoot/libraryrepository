﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using System.Threading;

namespace Library.Features.AuthorFeatures
{
    public class DeleteAuthorConfirmedF:IRequest<Unit>
    {
        public int Id;
        public class DeleteAuthorConfirmedHandler : IRequestHandler<DeleteAuthorConfirmedF, Unit>
        {
            private readonly IAuthorRepository _authorRepository;
            private readonly IUnitOfWork _unitOfWork;
            public DeleteAuthorConfirmedHandler(IAuthorRepository authorRepository, IUnitOfWork unitOfWork)
            {
                _authorRepository = authorRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<Unit> Handle(DeleteAuthorConfirmedF request, CancellationToken cancellationToken)
            {
                _authorRepository.Delete(request.Id);
                _unitOfWork.SaveContext();
                return default;
            }
        }
    }
}

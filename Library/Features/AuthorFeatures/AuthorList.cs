﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Library.Repositories;
using Library.ViewModels;
using AutoMapper;

namespace Library.Features.AuthorFeatures
{
    public class AuthorList : IRequest<IEnumerable<AuthorSelectListViewModel>>
    {
        public class AuthorListHandler : IRequestHandler<AuthorList, IEnumerable<AuthorSelectListViewModel>>
        {
            private readonly IMapper _mapper;
            private readonly IAuthorRepository _authorRepository;
            public AuthorListHandler(IMapper mapper,IAuthorRepository authorRepository)
            {
                _mapper = mapper;
                _authorRepository = authorRepository;
            }
            public async Task<IEnumerable<AuthorSelectListViewModel>> Handle(AuthorList request, CancellationToken cancellationToken)
            {
                IEnumerable<AuthorSelectListViewModel> authors = _mapper.Map<List<Author>, List<AuthorSelectListViewModel>>(await _authorRepository.GetLItem());
                return authors;
            }
        }
    }
}

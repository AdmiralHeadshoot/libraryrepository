﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using Library.ViewModels;
using MediatR;
using Library.Repositories;

namespace Library.Features.LibrarianFeatures
{
    public class GiveBookF:IRequest<Reservation>
    {
        public int id;
        public class GiveBookFHandler : IRequestHandler<GiveBookF, Reservation>
        {
            private readonly IReservationRepository _reservationRepository;
            private readonly IUnitOfWork _unitOfWork;
            public GiveBookFHandler(IReservationRepository reservationRepository,IUnitOfWork unitOfWork)
            {
                _reservationRepository = reservationRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<Reservation> Handle(GiveBookF request, CancellationToken cancellationToken)
            {
                Reservation reservation = _reservationRepository.GetItem(request.id);
                reservation.RecievmentTime = DateTime.Now;
                _reservationRepository.Update(reservation);
                _unitOfWork.SaveContext();
                return reservation;
            }
        }
    }
}

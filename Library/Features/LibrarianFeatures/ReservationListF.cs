﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.ViewModels;
using System.Threading;
using Library.Repositories;
using AutoMapper;
using Library.Models;

namespace Library.Features.LibrarianFeatures
{
    public class ReservationListF : IRequest<IEnumerable<ReservationListViewModel>>
    {
        public class ReservationListFHandler : IRequestHandler<ReservationListF, IEnumerable<ReservationListViewModel>>
        {
            private IReservationRepository _reservationRepository;
            private IMapper _mapper;
            public ReservationListFHandler(IReservationRepository reservationRepository, IMapper mapper)
            {
                _reservationRepository = reservationRepository;
                _mapper = mapper;
            }
            public async Task<IEnumerable<ReservationListViewModel>> Handle(ReservationListF request, CancellationToken cancellationToken)
            {
                IEnumerable<ReservationListViewModel> model = _mapper.Map<IEnumerable<Reservation>, IEnumerable<ReservationListViewModel>>(await _reservationRepository.GetReservationsList());
                return model;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using MediatR;
using Library.Repositories;
using Library.Services;
using Microsoft.Extensions.Configuration;

namespace Library.Features.LibrarianFeatures
{
    public class ReturnBookF:IRequest<Unit>
    {
        public int id;
        public class ReturnBookFHandler : IRequestHandler<ReturnBookF, Unit>
        {
            private readonly IReservationRepository _reservationRepository;
            private readonly IBookRepository _bookRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IConfiguration _configuration;
            private readonly ISubscriptionRepository _subscriptionRepository;
            public ReturnBookFHandler(IReservationRepository reservationRepository,IBookRepository bookRepository,IUnitOfWork unitOfWork,IConfiguration configuration,ISubscriptionRepository subscriptionRepository)
            {
                _reservationRepository = reservationRepository;
                _bookRepository = bookRepository;
                _unitOfWork = unitOfWork;
                _configuration = configuration;
                _subscriptionRepository = subscriptionRepository;
            }
            public async Task<Unit> Handle(ReturnBookF request, CancellationToken cancellationToken)
            {
                Reservation reservation = _reservationRepository.GetItem(request.id);
                reservation.ReturingTime = DateTime.Now;
                Book book = _bookRepository.GetItem(reservation.BookId);
                book.isAvailable = true;
                _reservationRepository.Update(reservation);
                _bookRepository.Update(book);
                _unitOfWork.SaveContext();
                IEnumerable<Subscription> subscriptions = _subscriptionRepository.GetItemList(book.Id);
                await SendSubscriptionsEmailAsync(subscriptions);
                return default;
            }
            public async Task SendSubscriptionsEmailAsync(IEnumerable<Subscription> subscriptions)
            {
                EmailService emailService = new EmailService(_configuration);
                foreach (var s in subscriptions)
                {
                    string email = s.User.Email;
                    string subject = "Книга, на которую вы были подписаны, доступна.";
                    string message = $"Книга {s.Book.Name} доступна для бронирования.";
                    await emailService.SendEmailAsync(email, subject, message);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Library.Models;
using Library.Repositories;
using Library.ViewModels;
using MediatR;

namespace Library.Features.LibrarianFeatures
{
    public class BookingListF : IRequest<IEnumerable<BookingsListViewModel>>
    {
        public class BookingListFHandler : IRequestHandler<BookingListF, IEnumerable<BookingsListViewModel>>
        {
            private IReservationRepository _reservationRepository;
            private IMapper _mapper;
            public BookingListFHandler(IReservationRepository reservationRepository, IMapper mapper)
            {
                _reservationRepository = reservationRepository;
                _mapper = mapper;
            }
            public async Task<IEnumerable<BookingsListViewModel>> Handle(BookingListF request, CancellationToken cancellationToken)
            {
                IEnumerable<BookingsListViewModel> model = _mapper.Map<IEnumerable<Reservation>, IEnumerable<BookingsListViewModel>>(await _reservationRepository.GetBookingsList());
                return model;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models;
using Library.Repositories;
using Library.ViewModels;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using AutoMapper;

namespace Library.Features.ReservationFeatures
{
    public class MyReservationsF : IRequest<MyPageViewModel>
    {
        public HttpContext httpContext;
        public class MyReservationHandler : IRequestHandler<MyReservationsF, MyPageViewModel>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly UserManager<User> _userManager;
            private readonly IReservationRepository _reservationRepository;
            private readonly ISubscriptionRepository _subscriptionRepository;
            private readonly IMapper _mapper;
            public MyReservationHandler(IUnitOfWork unitOfWork, UserManager<User> userManager, IReservationRepository reservationRepository, ISubscriptionRepository subscriptionRepository, IMapper mapper)
            {
                _unitOfWork = unitOfWork;
                _userManager = userManager;
                _reservationRepository = reservationRepository;
                _subscriptionRepository = subscriptionRepository;
                _mapper = mapper;
            }
            public async Task<MyPageViewModel> Handle(MyReservationsF request, CancellationToken cancellationToken)
            {
                string userId = _userManager.GetUserAsync(request.httpContext.User).Result.Id;
                User user = _userManager.Users.FirstOrDefault(u => u.Id == userId);
                IEnumerable<ReservationListViewModel> reservations = _mapper.Map<IEnumerable<Reservation>, IEnumerable<ReservationListViewModel>>(await _reservationRepository.GetReservationsList());
                IEnumerable<BookingsListViewModel> bookings = _mapper.Map<IEnumerable<Reservation>, IEnumerable<BookingsListViewModel>>(await _reservationRepository.GetBookingsList());



                //List<Reservation> reservations =  _reservationRepository.GetReservationsList(userId).ToList();
                //List<Reservation> bookings = _reservationRepository.GetBookingsList(userId).ToList();
                MyPageViewModel model = new MyPageViewModel
                {
                    Reservations = reservations,
                    Bookings=bookings
                    //Reservations = reservations,
                    //Bookings = bookings
                };
                return model;
            }
        }
    }
}

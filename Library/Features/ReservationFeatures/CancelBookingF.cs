﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using Library.ViewModels;
using System.Threading;
using Library.Models;

namespace Library.Features.ReservationFeatures
{
    public class CancelBookingF : IRequest<ReservationViewModel>
    {
        public int Id;
        public class CancelBookingFHandler : IRequestHandler<CancelBookingF, ReservationViewModel>
        {
            private IReservationRepository _reservationRepository;

            public CancelBookingFHandler(IReservationRepository reservationRepository)
            {
                _reservationRepository = reservationRepository;
            }
            public async Task<ReservationViewModel> Handle(CancelBookingF request, CancellationToken cancellationToken)
            {
                if (_reservationRepository.GetItem(request.Id) != null)
                {
                    Reservation reservation = _reservationRepository.GetItem(request.Id);
                    ReservationViewModel model = new ReservationViewModel
                    {
                        ReservationId = reservation.ReservationId
                    };
                    return model;
                }
                return null;
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.ViewModels;
using Library.Repositories;
using Library.Models;
using System.Threading;

namespace Library.Features.ReservationFeatures
{
    public class CancelBookingConfirmedF:IRequest<IEnumerable<Subscription>>
    {
        public int Id;
        public class CancelBookingConfirmedFHandler : IRequestHandler<CancelBookingConfirmedF, IEnumerable<Subscription>>
        {
            private IReservationRepository _reservationRepository;
            private IBookRepository _bookRepository;
            private IUnitOfWork _unitOfWork;
            private ISubscriptionRepository _subscriptionRepository;
            public CancelBookingConfirmedFHandler(IReservationRepository reservationRepository, IBookRepository bookRepository, IUnitOfWork unitOfWork,ISubscriptionRepository subscriptionRepository)
            {
                _reservationRepository = reservationRepository;
                _bookRepository = bookRepository;
                _unitOfWork = unitOfWork;
                _subscriptionRepository = subscriptionRepository;
            }

            public async Task<IEnumerable<Subscription>> Handle(CancelBookingConfirmedF request, CancellationToken cancellationToken)
            {
                Reservation reservation = _reservationRepository.GetItem(request.Id);
                Book book = _bookRepository.GetItem(reservation.BookId);
                book.isAvailable = true;
                _bookRepository.Update(book);
                _reservationRepository.Delete(request.Id);
                _unitOfWork.SaveContext();

                IEnumerable<Subscription> subscriptions = _subscriptionRepository.GetItemList(book.Id);
                return subscriptions;
            }
        }
    }
}

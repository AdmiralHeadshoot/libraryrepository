﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models;
using Library.ViewModels;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.Admin
{
    public class DeleteUser:IRequest<User>
    {
        public string Id;
        public class DeleteUserHandler : IRequestHandler<DeleteUser, User>
        {
            private UserManager<User> _userManager;
            public DeleteUserHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public async Task<User> Handle(DeleteUser request, CancellationToken cancellationToken)
            {
                User user = await _userManager.FindByIdAsync(request.Id);
                if (user != null)
                {
                    await _userManager.DeleteAsync(user);
                }
                return user;
            }
        }
    }
}

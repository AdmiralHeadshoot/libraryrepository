﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models;
using Library.ViewModels;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.Admin
{
    public class EditUserPost:IRequest<EditUserViewModel>
    {
        public EditUserViewModel model;
        public class EditUserPostHandler : IRequestHandler<EditUserPost, EditUserViewModel>
        {
            private readonly UserManager<User> _userManager;
            public EditUserPostHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public async Task<EditUserViewModel> Handle(EditUserPost request, CancellationToken cancellationToken)
            {
                User user = await _userManager.FindByIdAsync(request.model.Id);
                if (user != null)
                {
                    user.Email = request.model.Email;
                    user.UserName = request.model.Email;
                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return request.model;
                    }
                }
                return request.model;
            }
        }
    }
}

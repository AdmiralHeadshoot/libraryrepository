﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.ViewModels;
using Library.Models;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using AutoMapper.Configuration;
using Library.Services;

namespace Library.Features.Admin
{
    public class AddUser : IRequest<AddUserViewModel>
    {
        public AddUserViewModel model;
        public class AddUserHandler : IRequestHandler<AddUser, AddUserViewModel>
        {
            private readonly UserManager<User> _userManager;
            private readonly Microsoft.Extensions.Configuration.IConfiguration _configuration;
            //private readonly IConfiguration _configuration;
            public AddUserHandler(UserManager<User> userManager, Microsoft.Extensions.Configuration.IConfiguration configuration)
            {
                _userManager = userManager;
                _configuration = configuration;
            }
            public async Task<AddUserViewModel> Handle(AddUser request, CancellationToken cancellationToken)
            {
                User user = new User { Email = request.model.Email, UserName = request.model.Email, EmailConfirmed = true };
                string password = GeneratePassword();
                var result = await _userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    EmailService emailService = new EmailService(_configuration);
                    emailService.SendEmailAsync(request.model.Email, "Регистрация", $"Вас зарегистрировали в Библиотеке. Ваш пароль {password}");
                }
                return request.model;
            }
            public string GeneratePassword()
            {
                Random rnd = new Random();
                string pass = "";
                string[] arr = {
                "A","a","B","b","C","c","D","d","E","e","F","f","T","t","1","2","3","4","5","6","7","8","9","0"
            };
                for (int i = 0; i < 10; i++)
                {
                    pass = pass + arr[rnd.Next(0, 23)];
                }
                return pass;
            }
        }
    }
}

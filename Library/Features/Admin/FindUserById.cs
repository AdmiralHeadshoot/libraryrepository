﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models;
using System.Threading;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.Admin
{
    public class FindUserById:IRequest<User>
    {
        public string Id;
        public class FindUserByIdHandler : IRequestHandler<FindUserById, User>
        {
            private readonly UserManager<User> _userManager;
            public FindUserByIdHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public async Task<User> Handle(FindUserById request, CancellationToken cancellationToken)
            {
                User user = await _userManager.FindByIdAsync(request.Id);
                return user;
            }
        }
    }
}

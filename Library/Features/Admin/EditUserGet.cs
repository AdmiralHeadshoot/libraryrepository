﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.ViewModels;
using MediatR;
using Library.Services;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Library.Models;

namespace Library.Features.Admin
{
    public class EditUserGet:IRequest<EditUserViewModel>
    {
        public User user;
        public class EditUserHandler : IRequestHandler<EditUserGet, EditUserViewModel>
        {
            private readonly UserManager<User> _userManager;
            public EditUserHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public async Task<EditUserViewModel> Handle(EditUserGet request, CancellationToken cancellationToken)
            {
                EditUserViewModel model = new EditUserViewModel { Email = request.user.Email, Id = request.user.Id };
                return model;
            }
        }
    }
}

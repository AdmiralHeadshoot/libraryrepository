﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using Library.ViewModels;
using System.Threading;

namespace Library.Features.GenreFeatures
{
    public class DeleteGenreGetF:IRequest<GenreSelectListViewModel>
    {
        public int Id;
        public class DeleteGenreFHandler : IRequestHandler<DeleteGenreGetF, GenreSelectListViewModel>
        {
            private readonly IGenreRepository _genreRepository;
            public DeleteGenreFHandler(IGenreRepository genreRepository)
            {
                _genreRepository = genreRepository;
            }
            public async Task<GenreSelectListViewModel> Handle(DeleteGenreGetF request, CancellationToken cancellationToken)
            {
                var genre = _genreRepository.GetItem(request.Id);
                GenreSelectListViewModel model = new GenreSelectListViewModel
                {
                    GenreId = genre.GenreId,
                    GenreName=genre.GenreName
                };
                return model;
            }
        }
    }
}

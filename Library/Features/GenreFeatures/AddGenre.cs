﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using Library.ViewModels;
using Library.Models;
using System.Threading;

namespace Library.Features.GenreFeatures
{
    public class AddGenre:IRequest<GenreActionViewModel>
    {
        public Genre genre { get; set; }
        public class AddGenreGetHandler : IRequestHandler<AddGenre, GenreActionViewModel>
        {
            private IGenreRepository _genreRepository;
            private IUnitOfWork _unitOfWork;
            public AddGenreGetHandler(IGenreRepository genreRepository,IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
                _genreRepository = genreRepository;
            }
            public async Task<GenreActionViewModel> Handle(AddGenre request, CancellationToken cancellationToken)
            {
                GenreActionViewModel model = new GenreActionViewModel
                {
                    Id = request.genre.GenreId,
                    GenreName = request.genre.GenreName
                };
                _genreRepository.Create(request.genre);
                _unitOfWork.SaveContext();
                return model;
            }
        }
    }
}

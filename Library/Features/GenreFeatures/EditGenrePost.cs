﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.ViewModels;
using Library.Models;
using MediatR;
using System.Threading;
using Library.Repositories;

namespace Library.Features.GenreFeatures
{
    public class EditGenrePost:IRequest<Genre>
    {
        public Genre genre { get; set; }
        public class EditGenrePostHandler : IRequestHandler<EditGenrePost, Genre>
        {
            private IGenreRepository _genreRepository;
            private IUnitOfWork _unitOfWork;
            public EditGenrePostHandler(IUnitOfWork unitOfWork,IGenreRepository genreRepository)
            {
                _unitOfWork = unitOfWork;
                _genreRepository = genreRepository;
            }
            public async Task<Genre> Handle(EditGenrePost request, CancellationToken cancellationToken)
            {
                Genre genre = request.genre;
                _genreRepository.Update(genre);
                _unitOfWork.SaveContext();
                return genre;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Repositories;
using MediatR;
using Library.ViewModels;
using System.Threading;
using Library.Models;

namespace Library.Features.GenreFeatures
{
    public class EditGenreGet : IRequest<GenreActionViewModel>
    {
        public int Id { get; set; }
        public class EditGenreGetHandler : IRequestHandler<EditGenreGet, GenreActionViewModel>
        {
            private IGenreRepository _genreRepository;
            public EditGenreGetHandler(IGenreRepository genreRepository)
            {
                _genreRepository = genreRepository;
            }
            public async Task<GenreActionViewModel> Handle(EditGenreGet request, CancellationToken cancellationToken)
            {
                Genre genre = await _genreRepository.GetItemAsync(request.Id);
                GenreActionViewModel model = new GenreActionViewModel
                {
                    Id = genre.GenreId,
                    GenreName = genre.GenreName
                };
                if (genre == null)
                {
                    return null;
                }
                return model;
            }
        }
    }
}

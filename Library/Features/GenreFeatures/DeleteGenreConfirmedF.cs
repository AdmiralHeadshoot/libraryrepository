﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using Library.ViewModels;
using Library.Models;
using System.Threading;

namespace Library.Features.GenreFeatures
{
    public class DeleteGenreConfirmedF:IRequest<Unit>
    {
        public int Id;
        public class DeleteGenreConfirmedFHandler : IRequestHandler<DeleteGenreConfirmedF,Unit>
        {
            private readonly IGenreRepository _genreRepository;
            private readonly IUnitOfWork _unitOfWork;
            public DeleteGenreConfirmedFHandler(IGenreRepository genreRepository,IUnitOfWork unitOfWork)
            {
                _genreRepository = genreRepository;
                _unitOfWork = unitOfWork;
            }

            public Task<Unit> Handle(DeleteGenreConfirmedF request, CancellationToken cancellationToken)
            {
                _genreRepository.Delete(request.Id);
                _unitOfWork.SaveContext();
                return default;
            }
        }
    }
}

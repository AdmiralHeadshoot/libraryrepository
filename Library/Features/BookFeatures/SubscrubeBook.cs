﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Models;
using Library.Repositories;
using Library.ViewModels;
using System.Threading;

namespace Library.Features.BookFeatures.Queries
{
    public class SubscrubeBook : IRequest<BookSelectViewModel>
    {
        public string UserId;
        public int BookId;
        public class SubscrubeBookHandler : IRequestHandler<SubscrubeBook, BookSelectViewModel>
        {
            private IBookRepository _bookRepository;
            private ISubscriptionRepository _subscriptionRepository;
            private IUnitOfWork _unitOfWork;
            public SubscrubeBookHandler(IBookRepository bookRepository, ISubscriptionRepository subscriptionRepository, IUnitOfWork unitOfWork)
            {
                _bookRepository = bookRepository;
                _subscriptionRepository = subscriptionRepository;
                _unitOfWork = unitOfWork;
            }
            public async Task<BookSelectViewModel> Handle(SubscrubeBook request, CancellationToken cancellationToken)
            {
                Subscription subscription = new Subscription { UserId = request.UserId, BookId = request.BookId };
                _subscriptionRepository.Create(subscription);
                Book book = await _bookRepository.GetItemAsync(request.BookId);
                BookSelectViewModel model = new BookSelectViewModel
                {
                    NameBook = book.Name,
                    Id = book.Id
                };
                _unitOfWork.SaveContext();
                return model;
            }
        }
    }
}

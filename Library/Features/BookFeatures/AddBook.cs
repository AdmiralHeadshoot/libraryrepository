﻿using Library.Models;
using Library.Repositories;
using Library.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Features.BookFeatures.Queries
{
    public class AddBook : IRequest<AddBookViewModel>
    {
        public class AddBookHandler : IRequestHandler<AddBook, AddBookViewModel>
        {
            private readonly IPublisherRepository _publisherRepository;
            private readonly IAuthorRepository _authorRepository;
            private readonly IGenreRepository _genreRepository;              
            public AddBookHandler(IAuthorRepository authorRepository,IPublisherRepository publisherRepository,IGenreRepository genreRepository)
            {
                _authorRepository = authorRepository;
                _genreRepository = genreRepository;
                _publisherRepository = publisherRepository;
            }
            public async Task<AddBookViewModel> Handle(AddBook request, CancellationToken cancellationToken)
            {
                List<Author> authors = await _authorRepository.GetLItem();
                List<Publisher> publishers = await _publisherRepository.GetLItem();
                List<Genre> genres = await _genreRepository.GetLItem();

                AddBookViewModel model = new AddBookViewModel
                {
                    Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                    Publishers = new SelectList(publishers, "PublisherId", "PublisherName"),
                    Genres = new SelectList(genres, "GenreId", "GenreName"),
                };
                return model;
            }
        }
    }
}






﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Library.Features.BookFeatures.Queries
{
    public class UserAuthenticated:IRequest<User>
    {
        public User User;
        public class UserAuthenticatedHandler : IRequestHandler<UserAuthenticated, User>
        {
            private UserManager<User> _userManager;
            public HttpContext httpContext;
            public UserAuthenticatedHandler(UserManager<User> userManager)
            {
                _userManager = userManager;
            }
            public async Task<User> Handle(UserAuthenticated request, CancellationToken cancellationToken)
            {
                request.User = _userManager.GetUserAsync(httpContext.User).Result;
                return request.User;
            }
        }
    }
}

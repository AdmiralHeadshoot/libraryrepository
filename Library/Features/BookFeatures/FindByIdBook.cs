﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using Library.ViewModels;
using MediatR;
using Library.Repositories;

namespace Library.Features.BookFeatures.Queries
{
    public class FindByIdBook:IRequest<Book>
    {
        public int Id;
        public class FindByIdBookHandler : IRequestHandler<FindByIdBook, Book>
        {
            private IBookRepository _bookRepository;
            public FindByIdBookHandler(IBookRepository bookRepository)
            {
                _bookRepository = bookRepository;
            }
            public async Task<Book> Handle(FindByIdBook request, CancellationToken cancellationToken)
            {
                Book book = await _bookRepository.GetItemAsync(request.Id);
                return book;
            }
        }
    }
}

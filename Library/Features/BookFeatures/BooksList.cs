﻿using AutoMapper;
using Library.Models;
using Library.Repositories;
using Library.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Features.BookFeatures.Queries
{
    public class BooksList : IRequest<CatalogViewModel>
    {
        public int? AuthorId { get; set; }
        public int? PublisherId { get; set; }
        public int? GenreId { get; set; }
        public string SearchString { get; set; }
        public int page { get; set; }
        public User user { get; set; }
        public class BooksListHandler : IRequestHandler<BooksList, CatalogViewModel>
        {
            private readonly IBookRepository _bookRepository;
            private readonly IAuthorRepository _authorRepository;
            private readonly IPublisherRepository _publisherRepository;
            private readonly IGenreRepository _genreRepository;
            private readonly IMapper _mapper;
            public BooksListHandler(IMapper mapper, IBookRepository bookRepository, IAuthorRepository authorRepository, IGenreRepository genreRepository, IPublisherRepository publisherRepository)
            {
                _bookRepository = bookRepository;
                _authorRepository = authorRepository;
                _genreRepository = genreRepository;
                _publisherRepository = publisherRepository;
                _mapper = mapper;
            }
            public async Task<CatalogViewModel> Handle(BooksList request, CancellationToken cancellationToken)
            {
                int pageSize = 6;
                List<GenreSelectListViewModel> GenreList = _mapper.Map<List<Genre>, List<GenreSelectListViewModel>>(await _genreRepository.GetLItem());
                List<AuthorSelectListViewModel> AuthorList = _mapper.Map<List<Author>, List<AuthorSelectListViewModel>>(await _authorRepository.GetLItem());
                List<PublisherSelectListViewModel> PublisherList = _mapper.Map<List<Publisher>, List<PublisherSelectListViewModel>>(await _publisherRepository.GetLItem());
                //IEnumerable<Book> bookSelectsList = await _bookRepository.GetLItem();
                IEnumerable<BookSelectViewModel> bookSelectsList = _mapper.Map<List<Book>, List<BookSelectViewModel>>(await _bookRepository.GetLItem());

                if (request.AuthorId != null && request.AuthorId != 0)
                {
                    bookSelectsList = bookSelectsList.Where(b => b.AuthorId == request.AuthorId);
                }
                if (request.GenreId != null && request.GenreId != 0)
                {
                    bookSelectsList = bookSelectsList.Where(b => b.GenreId == request.GenreId);
                }
                if (request.PublisherId != null && request.PublisherId != 0)
                {
                    bookSelectsList = bookSelectsList.Where(b => b.PublisherId == request.PublisherId);
                }
                if (!String.IsNullOrEmpty(request.SearchString))
                {
                    bookSelectsList = bookSelectsList.Where(b => b.NameBook.Contains(request.SearchString, StringComparison.CurrentCultureIgnoreCase));
                }
                var count = bookSelectsList.Count();
                var bookItems = bookSelectsList.Skip((request.page - 1) * pageSize).Take(pageSize).ToList();

                CatalogViewModel catalogView = new CatalogViewModel
                {
                    PageViewModel = new PageViewModel(count, request.page, pageSize),
                    Books = bookItems,
                    BooksFilterViewModel = new BooksFilterViewModel(AuthorList, request.AuthorId, GenreList, request.GenreId, PublisherList, request.PublisherId, request.SearchString),
                    User = request.user
                };
                return catalogView;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using Library.ViewModels;
using MediatR;
using Library.Repositories;

namespace Library.Features.BookFeatures.Queries
{
    public class ReserveBook : IRequest<BookSelectViewModel>
    {
        public int Id { get; set; }
        public User user { get; set; }
        public Book book { get; set; }
        public class ReserveBookHandler : IRequestHandler<ReserveBook, BookSelectViewModel>
        {
            private IBookRepository _bookRepository;
            private IUnitOfWork _unitOfWork;
            private IReservationRepository _reservationRepository;
            public ReserveBookHandler(IBookRepository bookRepository, IUnitOfWork unitOfWork, IReservationRepository reservationRepository)
            {
                _bookRepository = bookRepository;
                _unitOfWork = unitOfWork;
                _reservationRepository = reservationRepository;
            }
            public async Task<BookSelectViewModel> Handle(ReserveBook request, CancellationToken cancellationToken)
            {
                Reservation reservation = new Reservation { BookId = request.Id, ReservationTime = DateTime.Now, User = request.user };
                _reservationRepository.Create(reservation);
                BookSelectViewModel model = new BookSelectViewModel
                {
                    Id = request.book.Id,
                    NameBook = request.book.Name
                };
                request.book.isAvailable = false;
                _bookRepository.Update(request.book);
                _unitOfWork.SaveContext();
                return model;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Library.Models;
using Library.ViewModels;
using MediatR;
using Library.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Library.Features.BookFeatures
{
    public class EditBook:IRequest<AddBookViewModel>
    {
        public int Id;
        public class EditBookHandler : IRequestHandler<EditBook, AddBookViewModel>
        {
            private IBookRepository _bookRepository;
            private IAuthorRepository _authorRepository;
            private IGenreRepository _genreRepository;
            private IPublisherRepository _publisherRepository;
            public EditBookHandler(IBookRepository bookRepository,IAuthorRepository authorRepository,IGenreRepository genreRepository,IPublisherRepository publisherRepository)
            {
                _bookRepository = bookRepository;
                _authorRepository = authorRepository;
                _genreRepository = genreRepository;
                _publisherRepository = publisherRepository;
            }
            public async Task<AddBookViewModel> Handle(EditBook request, CancellationToken cancellationToken)
            {
                Book book = await _bookRepository.GetItemAsync(request.Id);
                List<Author> authors = await _authorRepository.GetLItem();
                List<Publisher> publishers = await _publisherRepository.GetLItem();
                List<Genre> genres = await _genreRepository.GetLItem();

                AddBookViewModel model = new AddBookViewModel
                {
                    BookId = book.Id,
                    Name = book.Name,
                    AuthorId = book.AuthorId,
                    GenreId = book.GenreId,
                    PublisherId = book.PublisherId,

                    Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                    Publishers = new SelectList(publishers, "PublisherId", "PublisherName"),
                    Genres = new SelectList(genres, "GenreId", "GenreName")
                };
                return model;
            }
        }
    }
}

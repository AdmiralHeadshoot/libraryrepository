﻿using Library.Models;
using Library.Repositories;
using Library.Services;
using Library.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Features.BookFeatures.Queries
{
    public class AddBookPost:IRequest<Book>
    {
        public AddBookViewModel model { get; set; }
        public IFormFile file { get; set; }
        public class AddBookPostHandler : IRequestHandler<AddBookPost, Book>
        {
            private IUnitOfWork _unitOfWork;
            private IBookRepository _bookRepository;
            private IWebHostEnvironment _appEnvironvent;
            private IAuthorRepository _authorRepository;
            private IGenreRepository _genreRepository;
            private IPublisherRepository _publisherRepository;
            public AddBookPostHandler(IUnitOfWork unitOfWork,IPublisherRepository publisherRepository,IGenreRepository genreRepository,IAuthorRepository authorRepository,IBookRepository bookRepository,IWebHostEnvironment appEnvironvent)
            {
                _unitOfWork = unitOfWork;
                _bookRepository = bookRepository;
                _appEnvironvent = appEnvironvent;
                _genreRepository = genreRepository;
                _publisherRepository = publisherRepository;
                _authorRepository = authorRepository;
            }
            public async Task<Book> Handle(AddBookPost request, CancellationToken cancellationToken)
            {
                Author author;
                Genre genre;
                Publisher publisher;

                FileService fileService = new FileService();
                await fileService.AddFileAsync(request.file, "Images", _appEnvironvent);
                if (request.model.isNewAuthor)
                {
                    author = new Author { AuthorName = request.model.AuthorName };
                    _authorRepository.Create(author);
                }
                else
                {
                    author = _authorRepository.GetItem(request.model.AuthorId);
                }
                if (request.model.isNewGenre)
                {
                    genre = new Genre { GenreName = request.model.GenreName };
                    _genreRepository.Create(genre);
                }
                else
                {
                    genre = _genreRepository.GetItem(request.model.GenreId);
                }
                if (request.model.isNewPublisher)
                {
                    publisher = new Publisher { PublisherName = request.model.PublisherName };
                    _publisherRepository.Create(publisher);
                }
                else
                {
                    publisher = _publisherRepository.GetItem(request.model.PublisherId);
                }
                if (request.model.isNewAuthor || request.model.isNewGenre || request.model.isNewPublisher)
                {
                    _unitOfWork.SaveContext(); 
                }
                Book book = new Book
                {
                    Name=request.model.Name,
                    AuthorId=request.model.AuthorId,
                    PublisherId=request.model.PublisherId,
                    GenreId=request.model.GenreId,
                    Picture=request.file.FileName
                };
                _bookRepository.Create(book);
                _unitOfWork.SaveContext();
                return book;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.Repositories;
using System.Threading;

namespace Library.Features.BookFeatures
{
    public class DeleteBookConfirmedF:IRequest<Unit>
    {
        public int Id;
        public class DeleteBookConfirmedHandler : IRequestHandler<DeleteBookConfirmedF, Unit>
        {
            private readonly IBookRepository _bookRepository;
            private readonly IUnitOfWork _unitOfWork;
            public DeleteBookConfirmedHandler(IBookRepository bookRepository,IUnitOfWork unitOfWork)
            {
                _bookRepository = bookRepository;
                _unitOfWork = unitOfWork;
            }
            public Task<Unit> Handle(DeleteBookConfirmedF request, CancellationToken cancellationToken)
            {
                _bookRepository.Delete(request.Id);
                _unitOfWork.SaveContext();
                return default;
            }
        }
    }
}

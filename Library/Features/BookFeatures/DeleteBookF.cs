﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.ViewModels;
using System.Threading;
using Library.Repositories;
using Library.Models;

namespace Library.Features.BookFeatures
{
    public class DeleteBookF:IRequest<DeleteBookViewModel>
    {
        public int Id;
        public class DeleteBookFHandler : IRequestHandler<DeleteBookF, DeleteBookViewModel>
        {
            private readonly IBookRepository _bookRepository;
            public DeleteBookFHandler(IBookRepository bookRepository)
            {
                _bookRepository = bookRepository;
            }
            public async Task<DeleteBookViewModel> Handle(DeleteBookF request, CancellationToken cancellationToken)
            {
                if (_bookRepository.GetBookExtended(request.Id) != null)
                {
                    Book book = _bookRepository.GetBookExtended(request.Id);
                    DeleteBookViewModel model = new DeleteBookViewModel
                    {
                        BookId = book.Id,
                        BookName = book.Name,
                        AuthorName = book.Author.AuthorName,
                        PublisherName = book.Publisher.PublisherName,
                        GenreName = book.Genre.GenreName
                    };
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}

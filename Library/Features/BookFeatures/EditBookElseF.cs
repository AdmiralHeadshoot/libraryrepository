﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Library.ViewModels;
using System.Threading;
using Library.Repositories;
using Library.Models;

namespace Library.Features.BookFeatures
{
    public class EditBookElseF:IRequest<AddBookViewModel>
    {
        public AddBookViewModel model;
        public class EditBookElseFHandler : IRequestHandler<EditBookElseF, AddBookViewModel>
        {
            private readonly IBookRepository _bookRepository;
            private readonly IAuthorRepository _authorRepository;
            private readonly IGenreRepository _genreRepository;
            private readonly IPublisherRepository _publisherRepository;
            public EditBookElseFHandler(IBookRepository bookRepository, IAuthorRepository authorRepository, IGenreRepository genreRepository, IPublisherRepository publisherRepository)
            {
                _bookRepository = bookRepository;
                _authorRepository = authorRepository;
                _genreRepository = genreRepository;
                _publisherRepository = publisherRepository;
            }
            public async Task<AddBookViewModel> Handle(EditBookElseF request, CancellationToken cancellationToken)
            {
                Book book = await _bookRepository.GetItemAsync(request.model.BookId);
                var authors = _authorRepository.GetListItem();
                var publisher = _publisherRepository.GetListItem();
                var genres = _genreRepository.GetListItem();
                return request.model;
            }
        }
    }
}

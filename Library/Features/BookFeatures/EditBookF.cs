﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;
using MediatR;
using Library.ViewModels;
using System.Threading;
using Library.Repositories;

namespace Library.Features.BookFeatures
{
    public class EditBookF:IRequest<AddBookViewModel>
    {
        public AddBookViewModel model;
        public class EditBookFHandler : IRequestHandler<EditBookF, AddBookViewModel>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IBookRepository _bookRepository;
            private readonly IAuthorRepository _authorRepository;
            private readonly IGenreRepository _genreRepository;
            private readonly IPublisherRepository _publisherRepository;
            public EditBookFHandler(IUnitOfWork unitOfWork, IBookRepository bookRepository, IAuthorRepository authorRepository, IGenreRepository genreRepository, IPublisherRepository publisherRepository)
            {
                _unitOfWork = unitOfWork;
                _bookRepository = bookRepository;
                _authorRepository = authorRepository;
                _genreRepository = genreRepository;
                _publisherRepository = publisherRepository;
            }
            public async Task<AddBookViewModel> Handle(EditBookF request, CancellationToken cancellationToken)
            {
                Author author = new Author();
                Genre genre = new Genre();
                Publisher publisher = new Publisher();

                if (request.model.isNewAuthor)
                {
                    author = new Author { AuthorName = request.model.AuthorName };
                }
                else
                {
                    author = await _authorRepository.GetItemAsync(request.model.AuthorId);
                }

                if (request.model.isNewPublisher)
                {
                    publisher = new Publisher { PublisherName = request.model.PublisherName };
                    _publisherRepository.Create(publisher);
                }
                else
                {
                    publisher = await _publisherRepository.GetItemAsync(request.model.PublisherId);
                }

                if (request.model.isNewGenre)
                {
                    genre = new Genre { GenreName = request.model.GenreName };
                    _genreRepository.Create(genre);
                }
                else
                {
                    genre = await _genreRepository.GetItemAsync(request.model.GenreId);
                }

                if (request.model.isNewAuthor || request.model.isNewGenre || request.model.isNewPublisher)
                {
                    _unitOfWork.SaveContext();
                }
                Book book = await _bookRepository.GetItemAsync(request.model.BookId);
                book.Name = request.model.Name;
                book.PublisherId = request.model.PublisherId;
                book.AuthorId = request.model.AuthorId;
                book.GenreId = request.model.GenreId;

                _bookRepository.Update(book);
                _unitOfWork.SaveContext();

                return request.model;
            }
        }
    }
}

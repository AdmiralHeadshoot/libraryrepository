﻿using Library.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Library.Configurations
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasOne(a => a.Author).WithMany(b => b.Books).HasForeignKey(a => a.AuthorId);
            builder.HasOne(g => g.Genre).WithMany(b => b.Books).HasForeignKey(g => g.GenreId);
            builder.HasOne(p => p.Publisher).WithMany(b => b.Books).HasForeignKey(p => p.PublisherId);
            builder.Property(b => b.Name).HasMaxLength(50);
        }
    }
}

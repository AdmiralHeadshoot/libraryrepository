﻿using AutoMapper;
using Library.Models;
using Library.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace Library.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UsersListViewModel>().ForMember(dest => dest.UserId
            , opt => opt.MapFrom(u => u.Id))
                .ForMember(dest => dest.UserEmail, opt => opt.MapFrom(u => u.Email));

            CreateMap<Author, AuthorSelectListViewModel>().ForMember(dest => dest.AuthorId
            , opt => opt.MapFrom(a => a.AuthorId))
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(a => a.AuthorName));

            CreateMap<Genre, GenreSelectListViewModel>().ForMember(dest => dest.GenreId
           , opt => opt.MapFrom(a => a.GenreId))
               .ForMember(dest => dest.GenreName, opt => opt.MapFrom(a => a.GenreName));

            CreateMap<Publisher, PublisherSelectListViewModel>().ForMember(dest => dest.PublisherId
           , opt => opt.MapFrom(a => a.PublisherId))
               .ForMember(dest => dest.PublisherName, opt => opt.MapFrom(a => a.PublisherName));

            CreateMap<Reservation, ReservationListViewModel>().ForMember(dest => dest.BookName, opt => opt.MapFrom(r => r.Book.Name))
                .ForMember(dest => dest.ReservationTimeView, opt => opt.MapFrom(r => r.ReservationTime))
                .ForMember(dest => dest.ReservationId, opt => opt.MapFrom(r => r.ReservationId));

            CreateMap<Reservation, BookingsListViewModel>().ForMember(dest => dest.BookName, opt => opt.MapFrom(b => b.Book.Name))
                .ForMember(dest => dest.RecievmentTimeView, opt => opt.MapFrom(b => b.RecievmentTime))
                .ForMember(dest => dest.ReturingTimeView, opt => opt.MapFrom(b => b.ReturingTime))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(b => b.User.Email))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(b => b.ReservationId));

            CreateMap<Reservation, ReservationViewModel>().ForMember(dest => dest.ReservationId, opt => opt.MapFrom(r => r.ReservationId));

            CreateMap<Book, BookSelectViewModel>().ForMember(dest => dest.Id, opt => opt.MapFrom(b => b.Id))
                .ForMember(dest => dest.NameBook, opt => opt.MapFrom(b => b.Name))
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(b => b.Author.AuthorName))
                .ForMember(dest => dest.GenreName, opt => opt.MapFrom(b => b.Genre.GenreName))
                .ForMember(dest => dest.PublisherName, opt => opt.MapFrom(b => b.Publisher.PublisherName))
                .ForMember(dest => dest.Picture, opt => opt.MapFrom(b => b.Picture))
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(b => b.AuthorId))
                .ForMember(dest => dest.GenreId, opt => opt.MapFrom(b => b.GenreId))
                .ForMember(dest => dest.PublisherId, opt => opt.MapFrom(b => b.PublisherId))
                .ForMember(dest => dest.IsAvailable, opt => opt.MapFrom(b => b.isAvailable));

            CreateMap<IdentityRole, RolesListViewModel>().ForMember(dest => dest.Id, opt => opt.MapFrom(r => r.Id))
                .ForMember(dest => dest.RoleName, opt => opt.MapFrom(r => r.Name));
        }
    }
}

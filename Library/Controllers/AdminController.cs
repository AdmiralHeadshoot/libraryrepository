﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Library.ViewModels;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using Library.Features.Admin;
using MediatR;

namespace Library.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<User> _userManager;
        private IMediator _mediator;
        private IMapper _mapper;
        public AdminController(IMapper mapper, IMediator mediator, UserManager<User> userManager)
        {
            _mediator = mediator;
            _userManager = userManager;
            _mapper = mapper;
        }
        public IActionResult UsersList()
        {
            List<UsersListViewModel> userList = _mapper.Map<List<User>, List<UsersListViewModel>>(_userManager.Users.ToList());
            return View(userList);
        }
        [HttpGet]
        public IActionResult AddUser()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> AddUser(AddUserViewModel _model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mediator.Send(new AddUser() { model = _model });
            }
            return RedirectToAction("UsersList");
        }
        [HttpGet]
        public async Task<IActionResult> EditUser(string id)
        {
            User result = await _mediator.Send(new FindUserById() { Id = id });
            if (result == null)
            {
                return NotFound();
            }
            var model = await _mediator.Send(new EditUserGet() { user = result });
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    user.Email = model.Email;
                    user.UserName = model.Email;
                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("UsersList");
                    }
                    else
                    {
                        foreach (var Error in result.Errors)
                        {
                            ModelState.AddModelError("", Error.Description);
                        }
                    }
                }
            }
            return View(model);
        }
        public async Task<IActionResult> DeleteUser(string id)
        {
            await _mediator.Send(new DeleteUser() { Id = id });
            return RedirectToAction("UsersList");
        }
        public string GeneratePassword()
        {
            Random rnd = new Random();
            string pass = "";
            string[] arr = {
                "A","a","B","b","C","c","D","d","E","e","F","f","T","t","1","2","3","4","5","6","7","8","9","0"
            };
            for (int i = 0; i < 10; i++)
            {
                pass = pass + arr[rnd.Next(0, 23)];
            }
            return pass;
        }
    }
}

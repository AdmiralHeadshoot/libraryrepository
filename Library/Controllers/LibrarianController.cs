﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Library.Features.LibrarianFeatures;
using MediatR;

namespace Library.Controllers
{
    [Authorize(Roles = "librarian")]
    public class LibrarianController : Controller
    {
        private readonly IMediator _mediator;
        public LibrarianController(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<ActionResult> ReservationList()
        {
            var reservations = await _mediator.Send(new ReservationListF());
            return View(reservations);
        }
        public async Task<ActionResult> BookingList()
        {
            var reservations = await _mediator.Send(new BookingListF());
            return View(reservations);
        }
        public ActionResult GiveBook(int Id)
        {
            _mediator.Send(new GiveBookF() { id = Id });
            return RedirectToAction("ReservationList", "Librarian");
        }
        public IActionResult ReturnBook(int Id)
        {
            _mediator.Send(new ReturnBookF() { id = Id });
            return RedirectToAction("BookingList", "Librarian");
        }
    }
}

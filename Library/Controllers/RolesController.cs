﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Library.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Library.Features.RoleFeatures;
using MediatR;

namespace Library.Controllers
{
    [Authorize(Roles = "admin")]
    public class RolesController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IMediator _mediator;

        public RolesController(UserManager<User> userManager, IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
        }
        public async Task<IActionResult> RolesList()
        {
            var result = await _mediator.Send(new RoleListF());
            return View(result);
        }
        public IActionResult UsersList()
        {
            return View(_userManager.Users.ToList());
        }
        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateRole(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var roleNameResult = _mediator.Send(new FindByNameRole() { Name = name });
                IdentityResult roleCreateResult = await _mediator.Send(new CreateRoleF() { Name = name });
                if (roleCreateResult.Succeeded)
                {
                    return RedirectToAction("RolesList");
                }
                else
                {
                    foreach (var e in roleCreateResult.Errors)
                    {
                        ModelState.AddModelError(string.Empty, e.Description);
                    }
                }
            }
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> EditUserRole(string userId)
        {
            var EditResult = await _mediator.Send(new EditUserRoleGetF() { UserId = userId });
            if (EditResult.Id != null)
            {
                return View(EditResult);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> EditUserRole(string userId, List<string> roles)
        {
            var UserResult = await _mediator.Send(new EditUserRolePostF() { UserId = userId, Roles = roles });
            if (UserResult != null)
            {
                return RedirectToAction("UsersList", "Admin");
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> DeleteRole(string id)
        {
            await _mediator.Send(new DeleteRoleF() { Id = id });
            return RedirectToAction("RolesList");
        }
    }
}

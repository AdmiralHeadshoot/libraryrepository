﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Library.Models;
using Library.ViewModels;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using Library.Features.BookFeatures.Queries;
using Library.Features.Admin;

namespace Library.Controllers
{
    public class CatalogController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IMediator _mediatr;
        public CatalogController(IMediator mediator, UserManager<User> userManager)
        {
            _userManager = userManager;
            _mediatr = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> BooksList(int? AuthorId, int? PublisherId, int? GenreId, string SearchString = null)
        {
            User user;
            if (User.Identity.IsAuthenticated)
            {
                user = _userManager.GetUserAsync(HttpContext.User).Result;
            }
            else
            {
                user = null;
            }
            CatalogViewModel model = await _mediatr.Send(new BooksList()
            {
                page = 1,
                AuthorId = AuthorId,
                PublisherId = PublisherId,
                GenreId = GenreId,
                SearchString = SearchString,
                user = user
            }
            );
            return View(model);
        }
        [Authorize]
        public async Task<ActionResult> ReserveBookAsync(int bookId, string UserId)
        {
            if (UserId == null)
            {
                RedirectToAction("Login", "Account");
            }
            var bookResult = await _mediatr.Send(new FindByIdBook() { Id = bookId });
            var UserResult = _mediatr.Send(new FindUserById() { Id = UserId }).Result;
            if (!UserResult.EmailConfirmed)
            {
                RedirectToPage("ConfirmEmail");
            }
            var result = await _mediatr.Send(new ReserveBook() { Id = bookId, book = bookResult, user = UserResult });
            return View(result);
        }
        [Authorize]
        public async Task<ActionResult> SubscribeBookAsync(string userId, int bookId)
        {
            var resultBook = await _mediatr.Send(new SubscrubeBook() { UserId = userId, BookId = bookId });
            return View(resultBook);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Models;
using Library.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Library.Models.AccoutViewModel;
using Library.ViewModels;
using Microsoft.Extensions.Configuration;
using Library.Features.AccountFeatures;
using MediatR;

namespace Library.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMediator _mediator;
        public AccountController(IMediator mediator, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mediator = mediator;
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(new Register()
                {
                    registerModel = model,
                    httpContext = HttpContext,
                    _url = Url
                });
                return RedirectToAction("NeedEmailConfirmation");
            }
            return View(model);
        }
        public ActionResult NeedEmailConfirmation()
        {
            return View();
        }
        public ActionResult EmailConfirmed()
        {
            return View();
        }
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _userManager.FindByNameAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                return RedirectToAction("EmailConfirmed", "Account");
            }
            else
            {
                return View("Error");
            }
        }
        [HttpGet]
        public async Task<IActionResult> Login(string _returnUrl)
        {
            var result = await _mediator.Send(new LoginGet() { returnUrl = _returnUrl });

            return View(result);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel _model, string _returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = await _mediator.Send(new LoginPost() { model = _model, returnUrl = _returnUrl, _url = Url });
                if (!String.IsNullOrEmpty(_model.ReturnUrl) && Url.IsLocalUrl(_model.ReturnUrl))
                {
                    return Redirect(_model.ReturnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин или пароль");
                    return RedirectToAction("BooksList", "Catalog");
                }
            }
            else
            {
                ModelState.AddModelError("", "Неправильный логин или пароль");
            }

            return View(_model);

        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel _model)
        {
            if (ModelState.IsValid)
            {
                var user = await _mediator.Send(new FindByEmailUser() { model = _model });

                if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
                {
                    return View("ForgotPasswordConfirmation");
                }
                await _mediator.Send(new ForgotPassword() { model = _model, httpContext = HttpContext, _url = Url });
                if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
                {
                    return View("ForgotPasswordConfirmation");
                }
                return View("ForgotPasswordConfirmation");
            }
            return View(_model);
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("Error") : View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _mediator.Send(new FindUserResetPassword() { Email = model.Email });
            if (user == null)
            {
                return View("ResetPasswordConfirmation");
            }
            var result = await _mediator.Send(new ResetPasswordF() { user = user, Code = model.Code, Password = model.Password });
            if (result.Succeeded)
            {
                return View("ResetPasswordConfirmation");
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("BooksList", "Catalog");
        }
    }
}

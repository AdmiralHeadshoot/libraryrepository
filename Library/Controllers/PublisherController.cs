﻿using System.Threading.Tasks;
using Library.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Library.Features.PublisherFeatures;

namespace Library.Controllers
{
    [Authorize(Roles = "librarian")]
    public class PublisherController : Controller
    {
        private readonly IMediator _mediatR;
        public PublisherController(IMediator mediator)
        {
            _mediatR = mediator;
        }
        public async Task<ActionResult> PublisherListAsync()
        {
            return View(await _mediatR.Send(new PublisherList()));
        }
        [HttpGet]
        public ActionResult AddPublisher()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPublisher(Publisher Publisher)
        {
            _mediatR.Send(new AddPublisher { publisher = Publisher });
            return RedirectToAction("PublisherList");
        }
        [HttpGet]
        public async Task<ActionResult> EditPublisher(int id)
        {
            var result = await _mediatR.Send(new EditPublisherGet() { Id = id });
            if (result == null)
            {
                return NotFound();
            }
            return View(result);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPublisher(Publisher Publisher)
        {
            if (ModelState.IsValid)
            {
                await _mediatR.Send(new EditPublisherPost() { publisher = Publisher });
                return RedirectToAction("PublisherList");
            }
            return View(Publisher);
        }
        public async Task<ActionResult> DeletePublisher(int id)
        {
            var Publisher = await _mediatR.Send(new FindByIdPublisher() { Id = id });
            if (Publisher == null)
            {
                return NotFound();
            }
            return View(Publisher);
        }
        [HttpPost, ActionName("DeletePublisher")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await _mediatR.Send(new DeleteConfirmed() { Id = id });
            return RedirectToAction("PublisherList");
        }
    }
}

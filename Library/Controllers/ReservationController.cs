﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Library.Models;
using Microsoft.AspNetCore.Mvc;
using Library.Services;
using Microsoft.Extensions.Configuration;
using MediatR;
using Library.Features.ReservationFeatures;
namespace Library.Controllers
{
    public class ReservationController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IMediator _mediator;
        public ReservationController(IConfiguration configuration, IMediator mediator)
        {
            _configuration = configuration;
            _mediator = mediator;

        }
        public async Task<ActionResult> MyReservations()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }

            var model = await _mediator.Send(new MyReservationsF() { httpContext = HttpContext });
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> CancelBooking(int id)
        {
            var result = await _mediator.Send(new CancelBookingF() { Id = id });
            if (result != null)
            {
                return View(result);
            }
            return NotFound();
        }
        [HttpPost, ActionName("CancelBooking")]
        public ActionResult CancelBookingConfirmed(int id)
        {
            _mediator.Send(new CancelBookingConfirmedF() { Id = id });
            return RedirectToAction("MyReservations");
        }
    }
}

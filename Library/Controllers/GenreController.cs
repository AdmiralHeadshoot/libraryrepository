﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Library.Models;
using Library.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Library.Features.GenreFeatures;

namespace Library.Controllers
{
    [Authorize(Roles = "librarian")]
    public class GenreController : Controller
    {
        private readonly IGenreRepository _genreRepository;
        private IMediator _mediator;
        public GenreController(IMediator mediator, IGenreRepository genreRepository)
        {
            _mediator = mediator;
            _genreRepository = genreRepository;
        }
        public async Task<ActionResult> GenreListAsync()
        {
            List<Genre> genres = await _genreRepository.GetLItem();
            return View(genres);
        }
        public ActionResult CreateGenre()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateGenre(Genre _genre)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(new AddGenre() { genre = _genre });
            }
            return RedirectToAction("GenreList");
        }
        [HttpGet]
        public async Task<ActionResult> EditGenreAsync(int id)
        {
            return View(await _mediator.Send(new EditGenreGet() { Id = id }));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditGenreAsync(Genre _genre)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(new EditGenrePost() { genre = _genre });
            }
            return RedirectToAction("GenreList");
        }
        [HttpGet]
        public async Task<ActionResult> DeleteGenre(int id)
        {
            var GenreResult = await _mediator.Send(new DeleteGenreGetF() { Id = id });
            if (GenreResult == null)
            {
                return NotFound();
            }
            return View(GenreResult);
        }
        [HttpPost, ActionName("DeleteGenre")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteGenreConfirmed(int id)
        {
            _mediator.Send(new DeleteGenreConfirmedF() { Id = id });
            return RedirectToAction("GenreList");
        }
    }
}

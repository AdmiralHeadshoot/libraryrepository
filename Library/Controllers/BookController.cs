﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Library.ViewModels;
using Microsoft.AspNetCore.Http;
using MediatR;
using Library.Features.BookFeatures.Queries;
using Library.Features.BookFeatures;

namespace Library.Controllers
{
    [Authorize(Roles = "librarian")]
    public class BookController : Controller
    {
        private IMediator _mediator;
        public BookController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        public async Task<ActionResult> BookList(int? _AuthorId, int? _GenreId, int? _PublisherId, string _SearchString = null)
        {
            var result = await _mediator.Send(new LibrarianBooksList() { AuthorId = _AuthorId, GenreId = _GenreId, PublisherId = _PublisherId, SearchString = _SearchString });
            return View(result);
        }
        [HttpGet]
        public async Task<ActionResult> AddBookAsync()
        {
            return View(await _mediator.Send(new AddBook()));
        }
        [HttpPost]
        public async Task<ActionResult> AddBookAsync(AddBookViewModel _model, IFormFile _file)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(new AddBookPost()
                {
                    file = _file,
                    model = _model
                });
            }
            return RedirectToAction("BookList");
        }
        public async Task<ActionResult> EditBookAsync(int id)
        {
            if (ModelState.IsValid)
            {
                var model = await _mediator.Send(new EditBook() { Id = id });
                return View(model);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpPost]
        public async Task<ActionResult> EditBookAsync(AddBookViewModel _model)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(new EditBookF() { model = _model });
                return RedirectToAction("BookList");
            }
            else
            {
                await _mediator.Send(new EditBookElseF() { model = _model });
                return View(_model);
            }
        }
        [HttpGet]
        public async Task<ActionResult> DeleteBook(int id)
        {
            var result = await _mediator.Send(new DeleteBookF() { Id = id });
            if (result != null)
            {
                return View(result);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpPost, ActionName("DeleteBook")]
        public ActionResult DeleteBookConfirmed(int id)
        {
            _mediator.Send(new DeleteBookConfirmedF() { Id = id });
            return RedirectToAction("BookList");
        }
    }
}


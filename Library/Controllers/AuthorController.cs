﻿
using System.Threading.Tasks;
using Library.Models;
using Library.Repositories;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Library.Features.AuthorFeatures;

namespace Library.Controllers
{
    [Authorize(Roles = "librarian")]
    public class AuthorController : Controller
    {
        private IMediator _mediator;
        public AuthorController(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<ActionResult> AuthorList()
        {
            return View(await _mediator.Send(new AuthorList()));
        }
        [HttpGet]
        public ActionResult AddAuthor()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddAuthorAsync(Author author)
        {
            await _mediator.Send(new AddAuthor()
            {
                author = author
            });
            return RedirectToAction("AuthorList");
        }
        [HttpGet]
        public async Task<ActionResult> EditAuthor(int id)
        {
            return View(await _mediator.Send(new EditAuthorGet() { Id = id }));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAuthor(Author _author)
        {
            await _mediator.Send(new EditAuthorPost() { author = _author });
            return RedirectToAction("AuthorList");
        }
        [HttpGet]
        public async Task<ActionResult> DeleteAuthor(int id)
        {
            var authorResult = await _mediator.Send(new DeleteAuthorF() { Id = id });
            if (authorResult == null)
            {
                return NotFound();
            }
            return View(authorResult);
        }
        [HttpPost, ActionName("DeleteAuthor")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await _mediator.Send(new DeleteAuthorConfirmedF() { Id = id });
            return RedirectToAction("AuthorList");
        }
    }
}

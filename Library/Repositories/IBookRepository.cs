﻿using Library.Models;
using System.Collections.Generic;

namespace Library.Repositories
{
    public interface IBookRepository : IRepository<Book>
    {
        IEnumerable<Book> GetFilteredList(int? AuthorId, int? GenreId, int? PublisherId, string searchString = null);
        Book GetBookExtended(int id);
    }
}

﻿using Library.Models;
using System.Collections.Generic;

namespace Library.Repositories
{
    public interface ISubscriptionRepository : IRepository<Subscription>
    {
        IEnumerable<Subscription> GetItemList(int BookId);
    }
}

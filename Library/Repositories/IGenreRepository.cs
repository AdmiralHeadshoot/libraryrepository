﻿using Library.Models;

namespace Library.Repositories
{
    public interface IGenreRepository : IRepository<Genre>
    {
        Genre GetGenreWithBooks(int id);
    }
}

﻿using Library.Models;
using Library.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Library.Repositories
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        private DataContext context { get { return Context; } }
        public BookRepository(DataContext context)
             : base(context)
        {
        }
        public Book GetBookExtended(int id)
        {
            return context.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.Publisher).FirstOrDefault(b => b.Id == id);
        }

        public IEnumerable<Book> GetFilteredList(int? AuthorId, int? GenreId, int? PublisherId, string searchString = null)
        {
            IEnumerable<Book> booksSearch = context.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.Publisher);
            if (AuthorId != null && AuthorId != 0)
            {
                booksSearch = booksSearch.Where(b => b.AuthorId == AuthorId);
            }
            if (GenreId != null && GenreId != 0)
            {
                booksSearch = booksSearch.Where(b => b.GenreId == GenreId);
            }
            if (PublisherId != null && PublisherId != 0)
            {
                booksSearch = booksSearch.Where(b => b.PublisherId == PublisherId);
            }
            return booksSearch;
        }
    }
}

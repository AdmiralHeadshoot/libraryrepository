﻿using Library.Models;
using Library.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Library.Repositories
{
    public class GenreRepository : Repository<Genre>, IGenreRepository
    {
        private DataContext context { get { return Context; } }
        public GenreRepository(DataContext context)
            : base(context)
        {
        }

        public Genre GetGenreWithBooks(int id)
        {
            return context.Genres.Include(g => g.Books).FirstOrDefault(g => g.GenreId == id);
        }
    }
}

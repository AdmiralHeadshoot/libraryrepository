﻿using Library.Models;
using Library.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Library.Repositories
{
    public class AuthorRepository : Repository<Author>, IAuthorRepository
    {
        private DataContext context
        {
            get { return Context; }
        }
        public AuthorRepository(DataContext context)
            : base(context)
        {

        }
        public Author GetAuthorWithBooks(int id)
        {
            return context.Authors.Include(a => a.Books).FirstOrDefault(a => a.AuthorId == id);
        }
    }
}

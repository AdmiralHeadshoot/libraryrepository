﻿using Library.Persistence;
using System;

namespace Library.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;
        private bool disposed = false;

        public UnitOfWork(DataContext context)
        {
            _context = context;
            Authors = new AuthorRepository(_context);
            Reservations = new ReservationRepository(_context);
            Books = new BookRepository(_context);
            Genres = new GenreRepository(_context);
            Publishers = new PublisherRepository(_context);
            Subscriptions = new SubscriptionRepository(_context);
        }

        public IAuthorRepository Authors { get; }
        public IBookRepository Books { get; }
        public IGenreRepository Genres { get; }
        public IReservationRepository Reservations { get; }
        public IPublisherRepository Publishers { get; }
        public ISubscriptionRepository Subscriptions { get; set; }
        public void SaveContext()
        {
            _context.SaveChanges();
        }
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

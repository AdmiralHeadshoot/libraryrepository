﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetListItem();
        Task<List<T>> GetLItem();
        Task<T> GetItemAsync(int id);
        T GetItem(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}

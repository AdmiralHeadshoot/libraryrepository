﻿using Library.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Library.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DataContext Context;
        public Repository(DataContext _context)
        {
            Context = _context;
        }
        public void Create(T item)
        {
            Context.Set<T>().Add(item);
        }
        public void Delete(int id)
        {
            T item = Context.Set<T>().Find(id);
            if (item != null)
            {
                Context.Set<T>().Remove(item);
            }
        }
        public void Update(T item)
        {
            Context.Entry(item).State = EntityState.Modified;
        }
        public async Task<T> GetItemAsync(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }
        public T GetItem(int id)
        {
            return Context.Set<T>().Find(id);
        }
        public async Task<IEnumerable<T>> GetListItem()
        {
            return await Context.Set<T>().ToListAsync();
        }
        public async Task<List<T>> GetLItem()
        {
            return await Context.Set<T>().ToListAsync();
        }
    }
}

﻿using Library.Models;
using Library.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Library.Repositories
{
    public class PublisherRepository : Repository<Publisher>, IPublisherRepository
    {
        private DataContext context { get { return Context; } }
        public PublisherRepository(DataContext context)
             : base(context)
        {

        }

        public Publisher GetPublisherWithBooks(int id)
        {
            return context.Publishers.Include(p => p.Books).FirstOrDefault(p => p.PublisherId == id);
        }
    }
}

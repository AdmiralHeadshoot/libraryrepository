﻿using Library.Models;
using Library.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Library.Repositories
{
    public class SubscriptionRepository : Repository<Subscription>, ISubscriptionRepository
    {
        private DataContext context { get { return Context; } }
        public SubscriptionRepository(DataContext context)
            : base(context)
        {

        }
        public IEnumerable<Subscription> GetItemList(int BookId)
        {
            return context.Subscriptions.Include(s => s.Book).Include(s => s.User).Where(s => s.BookId == BookId);
        }
    }
}

﻿using Library.Models;
using Library.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public class ReservationRepository : Repository<Reservation>, IReservationRepository
    {
        private DataContext context
        {
            get { return Context; }
        }
        public ReservationRepository(DataContext context)
            : base(context)
        {

        }

        public IEnumerable<Reservation> GetReservationsList(string userId)
        {
            return context.Reservations.Where(r => r.ReservationTime != null && r.RecievmentTime == null).Include(r => r.User).Include(r => r.Book).Where(r => r.UserId == userId);
        }

        public IEnumerable<Reservation> GetTimeOutReservationsList()
        {
            return context.Reservations.Where(r => r.ReservationTime < DateTime.Now && r.RecievmentTime == null);
        }

        public async Task<IEnumerable<Reservation>> GetBookingsList()
        {
            return context.Reservations.Where(r => r.ReturingTime == null && r.RecievmentTime != null).Include(r => r.User).Include(r => r.Book);
        }

        public IEnumerable<Reservation> GetBookingsList(string userId)
        {
            return context.Reservations.Where(r => r.ReturingTime == null && r.RecievmentTime != null).Include(r => r.User).Include(r => r.Book).Where(r => r.UserId == userId);
        }

        public async Task<IEnumerable<Reservation>> GetReservationsList()
        {
            return context.Reservations.Where(r => r.ReservationTime != null && r.RecievmentTime == null).Include(r => r.User).Include(r => r.Book);
        }

    }
}

﻿using Library.Models;
using System;

namespace Library.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        void SaveContext();
    }
}

﻿using Library.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Repositories
{
    public interface IReservationRepository : IRepository<Reservation>
    {
        Task<IEnumerable<Reservation>> GetReservationsList();
        IEnumerable<Reservation> GetReservationsList(string userId);
        IEnumerable<Reservation> GetTimeOutReservationsList();
        Task<IEnumerable<Reservation>> GetBookingsList();
        IEnumerable<Reservation> GetBookingsList(string userId);
    }
}

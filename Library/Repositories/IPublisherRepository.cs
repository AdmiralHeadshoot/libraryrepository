﻿using Library.Models;

namespace Library.Repositories
{
    public interface IPublisherRepository : IRepository<Publisher>
    {
        Publisher GetPublisherWithBooks(int id);
    }
}
